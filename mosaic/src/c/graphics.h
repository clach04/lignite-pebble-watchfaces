#pragma once

#define NUMBER_SIZE GSize(5, 7)

#ifdef PBL_ROUND
#define AMOUNT_OF_SQUARES_X 23
#define AMOUNT_OF_SQUARES_Y 23
#define HEIGHT_AND_WIDTH 8
#define SQUARE_PADDING_LEFT_RIGHT 6
#define SQUARE_PADDING_TOP_BOTTOM 4
#define ANIMATION_DELAY 1
#else
#define AMOUNT_OF_SQUARES_X 15
#define AMOUNT_OF_SQUARES_Y 17
#define HEIGHT_AND_WIDTH 10
#define SQUARE_PADDING_LEFT_RIGHT 2
#define SQUARE_PADDING_TOP_BOTTOM 1
#define ANIMATION_DELAY 3
#endif

#ifdef PBL_ROUND
#define CENTER GPoint(90, 90)
#define WINDOW_SIZE GSize(180, 180)
#define IS_ROUND true
#else
#define CENTER GPoint(72, 84)
#define WINDOW_SIZE GSize(144, 168)
#define IS_ROUND false
#endif

typedef enum {
	TimeRangeNone,
	TimeRangeTopLeft,
	TimeRangeTopRight,
	TimeRangeBottomLeft,
	TimeRangeBottomRight
} TimeRange;

typedef struct {
	bool display;
	GColor colour;
	GPoint location;
	GPoint rawLocation;
	TimeRange timeRange;
	uint8_t heightAndWidth;
} Square;

void graphics_set_time(struct tm *t);
void graphics_shake_handler();
void graphics_init(Layer *window_layer);
void graphics_deinit();
void graphics_battery_handler(BatteryChargeState new_charge_state);
Square *square_create(int i, int i1);
void square_destroy(Square *square);