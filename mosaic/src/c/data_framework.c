#include <pebble.h>
#include "lignite.h"
#include "data_framework.h"

//#define DEBUG_DEFAULT_SETTINGS

void settings_setup();
void settings_finish();
Settings data_framework_get_default_settings();
void data_framework_save_settings(bool logResult);

Settings data_framework_local_settings;
SettingsChangeCallback settings_callbacks[AMOUNT_OF_SETTINGS_CALLBACKS];

uint32_t data_framework_checksum = 0;
bool userWas1x, doNotVibrate; //If the user used this face before 2.0

void data_framework_setup(){
    app_message_register_inbox_received(data_framework_inbox);
    app_message_register_inbox_dropped(data_framework_inbox_dropped);
    app_message_open(512, 512);

    settings_setup();
}

void data_framework_finish(){
    settings_finish();
}

void process_tuple(Tuple *t){
    int key = t->key;
    int value = t->value->int32;
    NSLog("Processing key %d with value %d", key, value);
    switch (key) {
        case APP_KEY_BTREALERT:
            data_framework_local_settings.btrealert = value;
            break;
        case APP_KEY_BTDISALERT:
            data_framework_local_settings.btdisalert = value;
            break;
        case APP_KEY_ANIMATION_TYPE:
            data_framework_local_settings.animationType = value;
            break;
        case APP_KEY_SHAKE_TYPE:
            data_framework_local_settings.shakeType = value;
            break;
        case APP_KEY_BATTERY_INDICATOR:
            data_framework_local_settings.batteryIndicator = value;
            break;
        case APP_KEY_OLD_STYLE:
            data_framework_local_settings.oldStyle = value;
            break;

        #ifdef PBL_COLOUR
        case APP_KEY_NUMBER_COLOUR:
            data_framework_local_settings.numbersColour = GColorFromHEX(data_framework_hex_string_to_uint(t->value->cstring));
            break;
        case APP_KEY_BACKGROUND_COLOUR:
            data_framework_local_settings.backgroundColour = GColorFromHEX(data_framework_hex_string_to_uint(t->value->cstring));
            break;
        #endif

        default:
            doNotVibrate = true;
            break;
    }
}

void data_framework_log_int(char *keyname, int value){
    NSLog("\"%s\": %d,", keyname, value);
}

void data_framework_log_string(char *keyname, char *value){
    NSLog("\"%s\": \"%s\",", keyname, value);
}

void data_framework_inbox(DictionaryIterator *iter, void *context){
    //NSLog("Got a message");
    Tuple *t = dict_read_first(iter);
    if(t){
        process_tuple(t);
    }
    while(t != NULL){
        t = dict_read_next(iter);
        if(t){
            process_tuple(t);
        }
    }


    data_framework_save_settings(LOG_ALL);

    for(int i = 0; i < AMOUNT_OF_SETTINGS_CALLBACKS; i++){
        //data_framework_log_settings_struct(data_framework_local_settings);
        settings_callbacks[i](data_framework_local_settings);
    }
    if(!doNotVibrate){
        //NSLog("Got actual settings");
        vibes_double_pulse();
    }
    doNotVibrate = false;
    //04d29190d174877478b3043b948ddc19
}

void data_framework_inbox_dropped(AppMessageResult reason, void *context){
    if(LOG_ALL){
        //NSLog("Message dropped, reason %d.", reason);
    }
}

void data_framework_register_settings_callback(SettingsChangeCallback callback, SettingsCallback callbackIdentity){
    settings_callbacks[callbackIdentity] = callback;
}

//Cheers to http://forums.getpebble.com/discussion/comment/132931#Comment_132931
unsigned int data_framework_hex_string_to_uint(char const* hexstring){
    unsigned int result = 0;
    char const *c = hexstring;
    unsigned char thisC;
    while((thisC = *c) != 0){
        thisC = toupper(thisC);
        result <<= 4;
        if(isdigit(thisC)){
            result += thisC - '0';
        }
        else if(isxdigit(thisC)){
            result += thisC - 'A' + 10;
        }
        else{
            APP_LOG(APP_LOG_LEVEL_DEBUG, "ERROR: Unrecognised hex character \"%c\"", thisC);
            return 0;
        }
        ++c;
    }
    return result;
}

const char * GColorsNames[]= {
    "GColorBlack", "GColorOxfordBlue", "GColorDukeBlue", "GColorBlue",
    "GColorDarkGreen", "GColorMidnightGreen", "GColorCobaltBlue", "GColorBlueMoon",
    "GColorIslamicGreen", "GColorJaegerGreen", "GColorTiffanyBlue", "GColorVividCerulean",
    "GColorGreen", "GColorMalachite", "GColorMediumSpringGreen", "GColorCyan",
    "GColorBulgarianRose", "GColorImperialPurple", "GColorIndigo", "GColorElectricUltramarine",
    "GColorArmyGreen", "GColorDarkGray", "GColorLiberty", "GColorVeryLightBlue",
    "GColorKellyGreen", "GColorMayGreen", "GColorCadetBlue", "GColorPictonBlue",
    "GColorBrightGreen", "GColorScreaminGreen", "GColorMediumAquamarine", "GColorElectricBlue",
    "GColorDarkCandyAppleRed", "GColorJazzberryJam", "GColorPurple", "GColorVividViolet",
    "GColorWindsorTan", "GColorRoseVale", "GColorPurpureus", "GColorLavenderIndigo",
    "GColorLimerick", "GColorBrass", "GColorLightGray", "GColorBabyBlueEyes",
    "GColorSpringBud", "GColorInchworm", "GColorMintGreen", "GColorCeleste",
    "GColorRed", "GColorFolly", "GColorFashionMagenta", "GColorMagenta",
    "GColorOrange", "GColorSunsetOrange", "GColorBrilliantRose", "GColorShockingPink",
    "GColorChromeYellow", "GColorRajah", "GColorMelon", "GColorRichBrilliantLavender",
    "GColorYellow", "GColorIcterine", "GColorPastelYellow", "GColorWhite"
};

const char* get_gcolor_text(GColor m_color){
	if(gcolor_equal(m_color, GColorClear))
		return "GColorClear";
	return GColorsNames[m_color.argb & 0x3F];
}

Settings data_framework_get_settings(){
    #ifdef DEBUG_DEFAULT_SETTINGS
    #pragma message("Default settings are being used")
    return data_framework_get_default_settings();
    #else
    return data_framework_local_settings;
    #endif
}

Settings data_framework_get_default_settings(){
    Settings defaults;

    defaults.btdisalert = true;
    defaults.btrealert = false;
    defaults.animationType = AnimationTypeDazzle;
    defaults.shakeType = ShakeTypeNone;
    defaults.batteryIndicator = false;
	defaults.backgroundColour = GColorWhite;
	defaults.numbersColour = GColorBlack;
    defaults.oldStyle = false;

    return defaults;
}

#define CURRENT_STORAGE_VERSION 2
#define STORAGE_VERSION_1 1

void data_framework_save_settings(bool logResult){
    int result = persist_write_data(SETTINGS_KEY, &data_framework_local_settings, sizeof(data_framework_local_settings));
    int result1 = persist_write_int(STORAGE_VERSION_KEY, CURRENT_STORAGE_VERSION);
    if(logResult){
        NSLog("Wrote %d bytes to %s's settings.", result + result1, APP_NAME);
    }
}

int data_framework_load_settings(bool logResult){
    int result = persist_read_data(SETTINGS_KEY, &data_framework_local_settings, sizeof(data_framework_local_settings));
    if(logResult){
        NSLog("Read %d bytes from %s's settings.", result, APP_NAME);
    }
    if(result < 0){
        data_framework_local_settings = data_framework_get_default_settings();
    }
    return result;
}

Settings data_framework_port_user_from_settings_1(){
    v1Settings oldSettings;
    persist_read_data(SETTINGS_KEY, &oldSettings, sizeof(oldSettings));

    Settings newSettings;
    newSettings.btdisalert = oldSettings.btrealert;
    newSettings.btrealert = oldSettings.btrealert;
    newSettings.animationType = oldSettings.animationType;
    newSettings.shakeType = oldSettings.shakeType;
    newSettings.batteryIndicator = oldSettings.batteryIndicator;
	newSettings.backgroundColour = oldSettings.backgroundColour;
	newSettings.numbersColour = oldSettings.numbersColour;

    newSettings.oldStyle = false;

    return newSettings;
}

void settings_setup() {
    uint8_t version = persist_read_int(STORAGE_VERSION_KEY);

    int result = data_framework_load_settings(LOG_ALL);

    //User was already using the watchface and then upgraded to 2.0
    if(result > 0 && version == 0){
        NSLog("Using version 0 settings");
        persist_write_bool(USER_WAS_1X_KEY, true);
        userWas1x = true;
        return;
    }
    //or, the user had a good version in place
    if(version >= CURRENT_STORAGE_VERSION){
        NSLog("Using good version settings");
        userWas1x = persist_read_bool(USER_WAS_1X_KEY);
        return;
    }
    else if(version == STORAGE_VERSION_1){
        NSLog("Using storage 1 settings");
        data_framework_local_settings = data_framework_port_user_from_settings_1();
        data_framework_save_settings(LOG_ALL);
        return;
    }

    NSLog("Using default settings");
    data_framework_local_settings = data_framework_get_default_settings();
}

void settings_finish(){
    data_framework_save_settings(LOG_ALL);
}