#pragma once

typedef enum Number {
    HOUR_LAYER = 0,
    MINUTE_LAYER,
    SECOND_LAYER
} Number;

void main_window_init();
Window *main_window_get_window();
void main_window_deinit();
void main_window_update_settings(Settings new_settings);
