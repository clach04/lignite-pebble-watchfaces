#include <pebble.h>
#include <math.h>
#include "lignite.h"
#include "main_window.h"

#define ANIMATION_LENGTH 600

Window *main_window;
Settings settings;
TextLayer *time_layers[2][3], *date_layer, *week_day_layer;
GBitmap *background_bitmap, *bluetooth_icon;
BitmapLayer *background_layer;
Layer *window_layer, *battery_graphics_layer, *bluetooth_graphics_layer;
Notification *disconnect_notification, *reconnect_notification;
BatteryChargeState batteryState;
int time_array[2];
bool flip[3] = {false, false, false};

bool isCurrentlyNight = false;

void animate_number(Number layer, char buffer[2]){
	int length = ANIMATION_LENGTH;

    if(layer == SECOND_LAYER && ((settings.mode == SLOT_MODE_MINUTELY) || (settings.nightmode == true && isCurrentlyNight))){
        return;
    }

    TextLayer *current = time_layers[flip[layer]][layer];
    TextLayer *next = time_layers[!flip[layer]][layer];

    text_layer_set_text(next, buffer);

	GRect current_frame = layer_get_frame(text_layer_get_layer(current));
	GRect out_frame = GRect(current_frame.origin.x, current_frame.origin.y-60, current_frame.size.w, current_frame.size.h);
	GRect in_frame = GRect(current_frame.origin.x, current_frame.origin.y+50, current_frame.size.w, current_frame.size.h);

    animate_layer(text_layer_get_layer(current), &current_frame, &out_frame, length, 10);
    animate_layer(text_layer_get_layer(next), &in_frame, &current_frame, length, 20);

    flip[layer] = !flip[layer];
}

void boot_tick_handler(struct tm *t){
    static char hour_buffer[] = "00";
	if(clock_is_24h_style()){
		strftime(hour_buffer, sizeof(hour_buffer), "%H", t);
	}
	else{
		strftime(hour_buffer, sizeof(hour_buffer), "%I", t);
	}
    text_layer_set_text(time_layers[0][HOUR_LAYER], hour_buffer);

	static char minute_buffer[] = "00";
	strftime(minute_buffer, sizeof(minute_buffer), "%M", t);
    text_layer_set_text(time_layers[0][MINUTE_LAYER], minute_buffer);

	static char second_buffer[] = "00";
	strftime(second_buffer, sizeof(second_buffer), "%S", t);
    text_layer_set_text(time_layers[0][SECOND_LAYER], second_buffer);

	if((t->tm_hour > 22 || t->tm_hour < 8) && settings.nightmode){
		isCurrentlyNight = true;
		snprintf(second_buffer, sizeof(second_buffer), "00");
	}

	static char date_buffer[] = "Get rickity rect in front of what";
	strftime(date_buffer, sizeof(date_buffer), settings.dateFormat[0], t);
	text_layer_set_text(date_layer, date_buffer);
}

void main_tick_handler(struct tm *t, TimeUnits units){
	static char hour_buffers[2][3] = { "00", "00" };
	if(clock_is_24h_style()){
		strftime(hour_buffers[!flip[HOUR_LAYER]], sizeof(hour_buffers[!flip[HOUR_LAYER]]), "%H", t);
	}
	else{
		strftime(hour_buffers[!flip[HOUR_LAYER]], sizeof(hour_buffers[!flip[HOUR_LAYER]]), "%I", t);
	}

	static char minute_buffers[2][3] = { "00", "00" };
	strftime(minute_buffers[!flip[MINUTE_LAYER]], sizeof(minute_buffers[!flip[MINUTE_LAYER]]), "%M", t);

	static char second_buffers[2][3] = { "00", "00" };
	strftime(second_buffers[!flip[SECOND_LAYER]], sizeof(second_buffers[!flip[SECOND_LAYER]]), "%S", t);

	static char date_buffer[] = "Get rickity rect in front of what";
	strftime(date_buffer, sizeof(date_buffer), settings.dateFormat[0], t);
	text_layer_set_text(date_layer, date_buffer);

	if((t->tm_hour > 22 || t->tm_hour < 8) && settings.nightmode){
		isCurrentlyNight = true;
		snprintf(second_buffers[!flip[SECOND_LAYER]], sizeof(second_buffers[!flip[SECOND_LAYER]]), "00");

		tick_timer_service_subscribe(MINUTE_UNIT, main_tick_handler);
	}
	else if(isCurrentlyNight){
		isCurrentlyNight = false;

		tick_timer_service_subscribe(SECOND_UNIT, main_tick_handler);
	}

	if(t->tm_sec == 0){
		animate_number(MINUTE_LAYER, minute_buffers[!flip[MINUTE_LAYER]]);
		if(t->tm_min == 0){
			animate_number(HOUR_LAYER, hour_buffers[!flip[HOUR_LAYER]]);
		}
	}
	animate_number(SECOND_LAYER, second_buffers[!flip[SECOND_LAYER]]);
}

void update_bluetooth_icon(bool connected){
    GRect current_frame = !connected ? layer_get_frame(bluetooth_graphics_layer) : GRect(101, 104, 36, 22);
    GRect next_frame = GRect(current_frame.origin.x, connected ? 52 : 0, current_frame.size.w, current_frame.size.h);

    animate_layer(bluetooth_graphics_layer, &current_frame, &next_frame, ANIMATION_LENGTH, 10);
}

void bluetooth_handler(bool connected){
	if(connected && settings.btrealert){
		if(disconnect_notification->is_live){
			notification_force_dismiss(disconnect_notification);
		}
		notification_push(reconnect_notification, 10000);
		vibes_double_pulse();
	}
	else if(!connected && settings.btdisalert){
		if(reconnect_notification->is_live){
			notification_force_dismiss(reconnect_notification);
		}
		notification_push(disconnect_notification, 10000);
		vibes_long_pulse();
	}
    update_bluetooth_icon(connected);
}

void notification_push_handler(bool pushed, uint8_t id){
	if(pushed){
		layer_remove_from_parent(window_layer);
	}
	else{
		if(disconnect_notification->is_live || reconnect_notification->is_live){
			return;
		}
		layer_add_child(window_get_root_layer(main_window), window_layer);
	}
}

void main_window_settings_callback(Settings new_settings){
	settings = new_settings;

    layer_mark_dirty(battery_graphics_layer);
    layer_mark_dirty(window_layer);

    text_layer_set_font(date_layer, settings.batteryBar ? fonts_get_system_font(FONT_KEY_GOTHIC_14) : fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));

    layer_set_frame(text_layer_get_layer(date_layer), settings.batteryBar ? GRect(0, 130, 144, 168) : GRect(0, 134, 144, 168));
    layer_set_frame(battery_graphics_layer, settings.date ? GRect(0, 0, 144, 168) : GRect(0, -9, 144, 168));

    layer_set_hidden(text_layer_get_layer(date_layer), !settings.date);
    layer_set_hidden(text_layer_get_layer(time_layers[0][SECOND_LAYER]), settings.mode == SLOT_MODE_MINUTELY);
    layer_set_hidden(text_layer_get_layer(time_layers[1][SECOND_LAYER]), settings.mode == SLOT_MODE_MINUTELY);
    layer_set_hidden(bluetooth_graphics_layer, settings.mode == SLOT_MODE_SECONDLY);

    update_bluetooth_icon(bluetooth_connection_service_peek());

    #ifdef PBL_COLOR
    for(int i = 0; i < 2; i++){
        for(int i1 = 0; i1 < 3; i1++){
            text_layer_set_text_color(time_layers[i][i1], settings.numberColour);
        }
    }
    #endif
}

void window_proc(Layer *layer, GContext *ctx){
	#ifdef PBL_COLOR
	graphics_context_set_fill_color(ctx, settings.accentColour);
	#else
	graphics_context_set_fill_color(ctx, GColorWhite);
	#endif
	graphics_fill_rect(ctx, GRect(0, 0, 144, 168), 0, GCornerNone);
}

void battery_graphics_proc(Layer *layer, GContext *ctx){
    if(!settings.batteryBar){
        return;
    }
	batteryState = battery_state_service_peek();
	for(int i = 0; i < 10; i++){
        GColor colour = GColorWhite;
        #ifdef PBL_COLOR
            if(i == 0){
                colour = GColorRed;
            }
            else if(i > 0 && i < 3){
                colour = GColorOrange;
            }
            else{
                colour = GColorGreen;
            }
        #endif
        GPoint position = GPoint(18+(i*12), 155);
        if(i+1 <= batteryState.charge_percent/10){
            graphics_context_set_fill_color(ctx, colour);
            graphics_fill_circle(ctx, position, 4);
        }
        else{
            graphics_context_set_stroke_color(ctx, colour);
            graphics_draw_circle(ctx, position, 4);
        }
    }
}

void bluetooth_graphics_proc(Layer *layer, GContext *ctx){
    #ifdef PBL_COLOR
    graphics_context_set_fill_color(ctx, settings.numberColour);
    graphics_context_set_stroke_color(ctx, gcolor_legible_over(settings.numberColour));
    graphics_context_set_antialiased(ctx, false);
    #else
    graphics_context_set_fill_color(ctx, GColorBlack);
    graphics_context_set_stroke_color(ctx, GColorWhite);
    #endif

    graphics_fill_circle(ctx, GPoint(9, 9), 9); //Circle
    graphics_draw_line(ctx, GPoint(6, 6), GPoint(9, 9)); //Top left arm
    graphics_draw_line(ctx, GPoint(6, 11), GPoint(9, 7)); //Bottom left arm
    graphics_draw_line(ctx, GPoint(9, 4), GPoint(9, 13)); //Centre stick
    graphics_draw_line(ctx, GPoint(9, 4), GPoint(12, 7)); //From top centre stick to top right corner
    graphics_draw_line(ctx, GPoint(12, 10), GPoint(9, 13)); //From bottom right corner to middle bottom left corner
    graphics_draw_line(ctx, GPoint(9, 8), GPoint(11, 8)); //Centre
    graphics_draw_line(ctx, GPoint(9, 9), GPoint(11, 9)); //Centre
}

void battery_handler(BatteryChargeState state){
	batteryState = state;
	layer_mark_dirty(battery_graphics_layer);
}

void main_window_load(Window *window){
	window_layer = layer_create(GRect(0, 0, 144, 168));
	layer_set_update_proc(window_layer, window_proc);
	layer_add_child(window_get_root_layer(window), window_layer);

	static char *buffers[] = {
		"12", "34", "56"
	};
    for(int i = 0; i < 2; i++){
    	for(int i1 = 0; i1 < 3; i1++){
            GRect rect = GRect(16 + (i1*38), 44 + (i*80), 36, 42);
    		time_layers[i][i1] = text_layer_init(rect, GTextAlignmentCenter, fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_IMPACT_28)));
            TextLayer *current = time_layers[i][i1];
    		text_layer_set_text(current, buffers[i1]);
    		#ifdef PBL_BW
    		text_layer_set_text_color(current, GColorBlack);
    		#endif
    		layer_add_child(window_layer, text_layer_get_layer(current));
    	}
    }

    bluetooth_graphics_layer = layer_create(GRect(101, 52, 20, 20));
    layer_set_update_proc(bluetooth_graphics_layer, bluetooth_graphics_proc);
    layer_add_child(window_layer, bluetooth_graphics_layer);

	#ifdef PBL_COLOR
	background_bitmap = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BACKGROUND);
	#else
	background_bitmap = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BACKGROUND_WHITE);
	#endif

	background_layer = bitmap_layer_create(GRect(0, 0, 144, 168));
	bitmap_layer_set_bitmap(background_layer, background_bitmap);
	#ifdef PBL_COLOR
	bitmap_layer_set_compositing_mode(background_layer, GCompOpSet);
	#else
	bitmap_layer_set_compositing_mode(background_layer, GCompOpOr);
	#endif
	layer_add_child(window_layer, bitmap_layer_get_layer(background_layer));

	#ifdef PBL_BW //It's fucking retarded I have to do this...
	BitmapLayer *background_layer_extra = bitmap_layer_create(GRect(0, 0, 144, 168));
	bitmap_layer_set_bitmap(background_layer_extra, gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BACKGROUND_BLACK));
	bitmap_layer_set_compositing_mode(background_layer_extra, GCompOpClear);
	layer_add_child(window_layer, bitmap_layer_get_layer(background_layer_extra));
	#endif

	battery_graphics_layer = layer_create(GRect(0, 0, 144, 168));
	layer_set_update_proc(battery_graphics_layer, battery_graphics_proc);
	layer_add_child(window_layer, battery_graphics_layer);

	date_layer = text_layer_init(GRect(0, 130, 144, 168), GTextAlignmentCenter, fonts_get_system_font(FONT_KEY_GOTHIC_14));
	layer_add_child(window_layer, text_layer_get_layer(date_layer));

    main_window_settings_callback(data_framework_get_settings());

    struct tm *t;
  	time_t temp;
  	temp = time(NULL);
  	t = localtime(&temp);
    boot_tick_handler(t);

	tick_timer_service_subscribe(SECOND_UNIT, main_tick_handler);

    GBitmap *bluetoothIcon = gbitmap_create_with_resource(RESOURCE_ID_LIGNITE_IMAGE_BLUETOOTH_ICON);

    disconnect_notification = notification_create(window);
    notification_set_icon(disconnect_notification, bluetoothIcon);
    notification_set_accent_colour(disconnect_notification, PBL_IF_COLOR_ELSE(GColorRed, GColorWhite));
    notification_set_contents(disconnect_notification, "Oh boy...", "I lost connection to\nyour phone, sorry!");

    reconnect_notification = notification_create(window);
    notification_set_icon(reconnect_notification, bluetoothIcon);
    notification_set_accent_colour(reconnect_notification, PBL_IF_COLOR_ELSE(GColorBlue, GColorWhite));
    notification_set_contents(reconnect_notification, "Woohoo!", "You are now\nconnected to your\nphone again!");

    notification_register_push_handler(notification_push_handler);

	data_framework_register_settings_callback(main_window_settings_callback, SETTINGS_CALLBACK_MAIN_WINDOW);
}

void main_window_unload(Window *window){
	//Maybe later...
}

void main_window_init(){
	main_window = window_create();
	window_set_background_color(main_window, GColorBlack);
	window_set_window_handlers(main_window, (WindowHandlers){
		.load = main_window_load,
		.unload = main_window_unload
	});
	bluetooth_connection_service_subscribe(bluetooth_handler);
	battery_state_service_subscribe(battery_handler);
}

Window *main_window_get_window(){
	return main_window;
}

void main_window_deinit(){
	window_destroy(main_window);
}
