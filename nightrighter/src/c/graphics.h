#pragma once
#define GRAPHICS_AMOUNT_OF_LAYERS 3
#include "lignite.h"

typedef enum GraphicsLayer {
	GRAPHICS_LAYER_BATTERY = 0,
	GRAPHICS_LAYER_OTHER,
	GRAPHICS_LAYER_KITT
} GraphicsLayer;

void graphics_init_layers(Layer *window_layer);
void graphics_deinit_layers();
void graphics_battery_graphics_proc(Layer *layer, GContext *ctx);
void graphics_other_graphics_proc(Layer *layer, GContext *ctx);
void graphics_set_tm(struct tm *t);
void graphics_set_battery_charge_state(BatteryChargeState new_state);
void graphics_set_connected(bool connected);
void graphics_set_animating(GraphicsLayer layer, bool anim);
void graphics_update_settings(Settings new_settings);
void graphics_force_update_kitt();
