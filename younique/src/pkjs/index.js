var CLEAR_DAY = 0;
var CLEAR_NIGHT = 1;
var WINDY = 2;
var COLD = 3;
var PARTLY_CLOUDY_DAY = 4;
var PARTLY_CLOUDY_NIGHT = 5;
var HAZE = 6;
var CLOUD = 7;
var RAIN = 8;
var SNOW = 9;
var HAIL = 10;
var CLOUDY = 11;
var STORM = 12;
var NA = 13;

var imageId = {
  0 : STORM, //tornado
  1 : STORM, //tropical storm
  2 : STORM, //hurricane
  3 : STORM, //severe thunderstorms
  4 : STORM, //thunderstorms
  5 : HAIL, //mixed rain and snow
  6 : HAIL, //mixed rain and sleet
  7 : HAIL, //mixed snow and sleet
  8 : HAIL, //freezing drizzle
  9 : RAIN, //drizzle
  10 : HAIL, //freezing rain
  11 : RAIN, //showers
  12 : RAIN, //showers
  13 : SNOW, //snow flurries
  14 : SNOW, //light snow showers
  15 : SNOW, //blowing snow
  16 : SNOW, //snow
  17 : HAIL, //hail
  18 : HAIL, //sleet
  19 : HAZE, //dust
  20 : HAZE, //foggy
  21 : HAZE, //haze
  22 : HAZE, //smoky
  23 : WINDY, //blustery
  24 : WINDY, //windy
  25 : COLD, //cold
  26 : CLOUDY, //cloudy
  27 : CLOUDY, //mostly cloudy (night)
  28 : CLOUDY, //mostly cloudy (day)
  29 : PARTLY_CLOUDY_NIGHT, //partly cloudy (night)
  30 : PARTLY_CLOUDY_DAY, //partly cloudy (day)
  31 : CLEAR_NIGHT, //clear (night)
  32 : CLEAR_DAY, //sunny
  33 : CLEAR_NIGHT, //fair (night)
  34 : CLEAR_DAY, //fair (day)
  35 : HAIL, //mixed rain and hail
  36 : CLEAR_DAY, //hot
  37 : STORM, //isolated thunderstorms
  38 : STORM, //scattered thunderstorms
  39 : STORM, //scattered thunderstorms
  40 : STORM, //scattered showers
  41 : SNOW, //heavy snow
  42 : SNOW, //scattered snow showers
  43 : SNOW, //heavy snow
  44 : CLOUD, //partly cloudy
  45 : STORM, //thundershowers
  46 : SNOW, //snow showers
  47 : STORM, //isolated thundershowers
  3200 : NA, //not available
};

//console.log("Options " + localStorage.getItem('options'));
var options = JSON.parse(localStorage.getItem('options'));

if (options === null){
    options = {
        "custom_location" : "",
        "use_celsius" : false
    };
    //console.log("Celsuis has been set manually!");
}
else if(options.custom_location === undefined){
    options.custom_location = "";
    options.use_celsius = false;

    //console.log("Celsuis has been set manually.");
}

var xhrRequest = function (url, type, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        callback(this.responseText);
    };
    xhr.open(type, url);
    xhr.send();
};

function getWeatherFromLatLong(latitude, longitude) {
    var query = encodeURI('select woeid from geo.places where text="(' + latitude + ',' + longitude + ')" limit 1');
    var url = "http://query.yahooapis.com/v1/public/yql?q=" + query + "&format=json";

    //console.log("Reverse geocoding url: " + url);

    xhrRequest(url, 'GET',
        function(responseText) {
            var json = JSON.parse(responseText);
            var location = json.query.results.place.woeid;

            getWeatherFromWoeid(location);
        }
    );
}

function getWeatherFromLocation(location_name) {
  var response;
  var woeid = -1;

  var query = encodeURI("select woeid from geo.places(1) where text=\"" + location_name + "\"");
  var url = "http://query.yahooapis.com/v1/public/yql?q=" + query + "&format=json";
  //console.log(url);
  var req = new XMLHttpRequest();
  req.open('GET', url, true);
  req.onload = function(e) {
    if (req.readyState == 4) {
      if (req.status == 200) {
        //console.log(req.responseText);
        response = JSON.parse(req.responseText);
        if (response) {
          woeid = response.query.results.place.woeid;
          //console.log("Got WOEID of " + woeid);
          getWeatherFromWoeid(woeid);
        }
      } else {
        //console.log("Error in getting weather from location: " + e);
      }
    }
  };
  req.send(null);
}

function getWeatherFromWoeid(woeid) {
  var query = encodeURI("select item.condition from weather.forecast where woeid = " + woeid +
                        " and u = " + (false ? "\"c\"" : "\"f\""));
  var url = "http://query.yahooapis.com/v1/public/yql?q=" + query + "&format=json";

  var response;
  var req = new XMLHttpRequest();
  //console.log("Fetching weather from " + url);
  req.open('GET', url, true);
  req.onload = function(e) {
    if (req.readyState == 4) {
      if (req.status == 200) {
          //console.log("got weather data " + req.responseText);
        response = JSON.parse(req.responseText);
        if (response) {
          var condition = response.query.results.channel.item.condition;
          var conditionCode = imageId[condition.code];
          //console.log("temp " + condition.temp);
          //console.log("condition " + condition.code);
          //console.log("condition " + condition.text);

          var fixedTemp = parseInt(condition.temp);
          if(options.use_celsius){
              //console.log("User is using the best temperature scale");
              fixedTemp -= 32;
              fixedTemp = Math.ceil(fixedTemp*(5/9));
          }
          var message = {
            "condition" : conditionCode,
            "temperature" : fixedTemp
          };
       //console.log("Sending message through WOEID " + JSON.stringify(message));
          Pebble.sendAppMessage(message);
        }
      } else {
        //console.log("Error from getting through WOEID: " + e);
      }
    }
  };
  req.send(null);
}

var locationOptions = {
  "timeout": 15000,
  "maximumAge": 60000
};

function updateWeather() {
  if (options.custom_location.length < 1) {
    navigator.geolocation.getCurrentPosition(locationSuccess,
                                                    locationError,
                                                    locationOptions);
  } else {
    getWeatherFromLocation(options.custom_location);
  }
}

function locationSuccess(pos) {
  var coordinates = pos.coords;
  getWeatherFromLatLong(coordinates.latitude, coordinates.longitude);
}

function locationError(err) {
  console.warn('location error (' + err.code + '): ' + err.message);
  Pebble.sendAppMessage({
    "condition":11,
    "temperature":""
  });
}

var MessageQueue = require('./message-queue');

var APP_NAME = "younique";
var OPTIONS = "options";
var APP_UUID = "b06489ef-6e82-4e7b-bbe7-6d6e70a1ca9f";

var settingsURL = ("https://www.lignite.me/pebble/settings/" + APP_NAME);

var chunk_size = 500;
var overridePhotoSending = true;

var WATCH_PLATFORM = "";
var WATCH_IS_CHALK = false;

function successFunction() {
    //console.log("Success sending");
    if(overridePhotoSending){
        updateWeather();
        overridePhotoSending = false;
    }
}

function failureFunction(e) {
    //console.log("Failed sending " + e);
}

//http://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array-in-javascript
function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}

function savePhotoSequence(newSequence){
    localStorage["photoSequence_" + WATCH_PLATFORM] = JSON.stringify({
        indexes: newSequence
    });
}

function generatePhotoSequence(){
    var photos = getPhotos();
    var indexes = [];
    for(var i = 0; i < photos.length; i++){
        indexes.push(i);
    }
    shuffle(indexes);
    savePhotoSequence(indexes);
    return indexes;
}

function getPhotoSequence(){
    var indexes = localStorage["photoSequence_" + WATCH_PLATFORM];
    //console.log("Got " + indexes);
    var generateNewSequence = true;
    if (indexes) {
        indexes = JSON.parse(localStorage["photoSequence_" + WATCH_PLATFORM]).indexes;
        if(indexes.length !== 0){
            generateNewSequence = false;
        }
    }

    if(generateNewSequence) {
        indexes = generatePhotoSequence();
    }
    //console.log("Got " + indexes.length + " items lined up in sequence " + indexes);

    return indexes;
}

function savePhotos(photos) {
    localStorage["savedPhotosYounique_" + WATCH_PLATFORM] = JSON.stringify({
        photos: photos
    });

    generatePhotoSequence();
}

function getPhotos() {
    var savedPhotos = undefined;
    if (localStorage["savedPhotosYounique_" + WATCH_PLATFORM]) {
        //console.log("Loading.");
        savedPhotos = JSON.parse(localStorage["savedPhotosYounique_" + WATCH_PLATFORM]).photos;
    } else {
        //console.log("Creating.");
        savedPhotos = [];
        savePhotos(savedPhotos);
    }
    //console.log("Got " + savedPhotos.length + " photos.");

    return savedPhotos;
}

function savePhotoObjectToIndex(photoObject, indexToSave) {
    var photos = getPhotos();
    photos[indexToSave] = photoObject;
    //console.log("Saving to specific index " + indexToSave + " with length " + photos.length + ".");
    savePhotos(photos);
}

function sendRandomPhoto() {
    var photos = getPhotos();
    if (photos.length > 0) {
        var sequence = getPhotoSequence();
        if(sequence.length === 0){
            sequence = generatePhotoSequence();
        }
        var photoToSend = sequence[0];
        sequence.splice(0, 1); //Get rid of the first item in the sequence
        savePhotoSequence(sequence);

        //console.log("Sending photo at index " + photoToSend);

        //console.log("photoToSend " + photoToSend + " exists ? " + (!photos[photoToSend]));

        if(!photos[photoToSend]){
            console.error("Photo doesn't exist for some reason...");
            return;
        }

        var photo = photos[photoToSend];
        transferImageBytes(photo.photoBinaryData.leftHalf, false, chunk_size, function(){
            transferImageBytes(photo.photoBinaryData.rightHalf, true, chunk_size, successFunction, failureFunction);
        }, failureFunction);
    }
}

Pebble.addEventListener('showConfiguration', function(e) {
    Pebble.openURL(settingsURL);
});

function transferImageBytes(bytes, isRightHalf, chunkSize, successCb, failureCb) {
    var retries = 0;

    success = function() {
        if (successCb != undefined) {
            successCb();
        }
    };
    failure = function(e) {
        if (failureCb != undefined) {
            failureCb(e);
        }
    };

    sendChunk = function(start) {
        var txbuf = bytes.slice(start, start + chunkSize);
        Pebble.sendAppMessage({
                "photoTransferData": txbuf
            },
            function(e) {
                if (bytes.length > start + chunkSize) {
                    sendChunk(start + chunkSize);
                } else {
                    Pebble.sendAppMessage({
                        "photoTransferEnd": "done"
                    }, success, failure);
                }
            },
            function(e) {
                if (retries++ < 3) {
                    sendChunk(start);
                } else {
                    failure(e);
                }
            }
        );
    };

    Pebble.sendAppMessage({
            "photoTransferBegin": bytes.length,
            "photoTransferIsRightHalf": isRightHalf
        },
        function(e) {
            sendChunk(0);
        }, failure);
}

Pebble.addEventListener('webviewclosed', function(e) {
    if (e.response) {
        //console.log("Got " + e.response);
        var json = JSON.parse(e.response);
        if (json) {
            if(json["gs-date_format"] !== undefined){
                json["dateFormat"] = json["gs-date_format"];
                json["gs-date_format"] = undefined;
            }
            if(json.photos !== undefined){
                for(var i = 0; i < json.photos.length; i++){
                    var photoHandleObject = json.photos[i];
                    if(photoHandleObject.deleteImageAtIndex !== undefined){
                        var photos = getPhotos();
                        photos.splice(photoHandleObject.deleteImageAtIndex, 1);
                        savePhotos(photos);
                    }
                    else{
                        savePhotoObjectToIndex({
                            photoBinaryData: photoHandleObject.imageData,
                            photoIndex: photoHandleObject.imageIndex
                        }, photoHandleObject.imageIndex);
                    }
                }
                json.photos = undefined;
            }
            json.checksum = parseInt(json.checksum);
            if(json.modules !== undefined){
                for(var i = 0; i < (WATCH_IS_CHALK ? 3 : 6); i++){
                    if(i >= json.modules.length){
                        json.modules.push({
                            infoModuleDataType: 0,
                            infoModuleDisplayType: 0,
                            infoModuleDataInfo: 0,
                            infoModuleEnabled: 0,
                            infoModuleIndex: i
                        });
                        //console.log("Inserted manually.");
                    }
                    MessageQueue.sendAppMessage(json.modules[i]);
                }
                json.modules = undefined;
            }
            if(json.use_celsius !== undefined){
                options.use_celsius = json.use_celsius;
                updateWeather();
            }
            localStorage.setItem(OPTIONS, e.response);
            //console.log("Set options as " + e.response);
            options = JSON.parse(e.response);
            Pebble.sendAppMessage(json);
            if (options.email) {
                localStorage[EMAIL_STORAGE] = options.email;
            }
            if (options.accessCode) {
                localStorage[ACCESS_CODE_STORAGE] = options.accessCode;
            }
        }
    }

    overridePhotoSending = true;

    sendRandomPhoto();
});

Pebble.addEventListener('ready', function(e) {
    WATCH_PLATFORM = Pebble.getActiveWatchInfo().platform;
    WATCH_IS_CHALK = ("chalk" === WATCH_PLATFORM);

    //console.log("Ready.");
    setTimeout(sendRandomPhoto, 500);
    setInterval(updateWeather, 1800000);
});

Pebble.addEventListener('appmessage', function(e) {
    if (e.payload.photoTransferRequestedChunkSize) {
        chunk_size = e.payload.photoTransferRequestedChunkSize;
        //console.log("Got chunk size of " + chunk_size);
    }
    if(e.payload.pleaseGiveMeANewPhoto){
        sendRandomPhoto();
    }
});

/*
Hi!

Can you please not bother our APIs or anything?
It's ok to snoop around, though we kindly ask that you don't try to do any harm.
And if any exploits are found, email us: contact@lignite.io, we'd love to talk
and maybe even work something out!

Thank you :)
-Team Lignite
*/