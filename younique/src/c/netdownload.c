#include "netdownload.h"

NetDownloadContext* netdownload_create_context(NetDownloadCallback callback) {
    NetDownloadContext *ctx = malloc(sizeof(NetDownloadContext));

    ctx->length = 0;
    ctx->index = 0;
    ctx->data = NULL;
    ctx->callback = callback;

    return ctx;
}

void netdownload_destroy_context(NetDownloadContext *ctx) {
    if (ctx->data) {
        free(ctx->data);
    }
    free(ctx);
}

void netdownload_initialize(NetDownloadCallback callback) {
    NetDownloadContext *ctx = netdownload_create_context(callback);
    app_message_set_context(ctx);
}

void netdownload_destroy(NetDownload *image) {
    if (image) {
        free(image->data);
        free(image);
    }
}

void netdownload_setup(NetDownloadContext *ctx, Tuple *tuple){
    //APP_LOG(APP_LOG_LEVEL_DEBUG, "Start transmission. Size=%lu, heap free %d", tuple->value->uint32, heap_bytes_free());

    ctx->callback(NULL, 0);

    if (ctx->data != NULL) {
        free(ctx->data);
    }
    ctx->data = malloc(tuple->value->uint32);
    if (ctx->data != NULL) {
        ctx->length = tuple->value->uint32;
        ctx->index = 0;
    }
    else {
        //APP_LOG(APP_LOG_LEVEL_WARNING, "Unable to allocate memory to receive image.");
        ctx->length = 0;
        ctx->index = 0;
    }
}

void netdownload_handle_new_data(Tuple *tuple, DictionaryIterator *iter, void *context) {
    NetDownloadContext *ctx = (NetDownloadContext*) context;

    if (!tuple) {
        return;
    }
    if(tuple->key == NETDL_DATA){
        if (ctx->index + tuple->length <= ctx->length) {
            memcpy(ctx->data + ctx->index, tuple->value->data, tuple->length);
            ctx->index += tuple->length;
        }
    }
    else if(tuple->key == NETDL_BEGIN || tuple->key == NETDL_IS_RIGHT_HALF){
        if(tuple->key == NETDL_IS_RIGHT_HALF){
            ctx->right_half = tuple->value->uint8;
            Tuple *data_tuple = dict_find(iter, NETDL_BEGIN);
            netdownload_setup(ctx, data_tuple);
        }
        else{
            netdownload_setup(ctx, tuple);
            Tuple *right_half_tuple = dict_find(iter, NETDL_IS_RIGHT_HALF);
            ctx->right_half = right_half_tuple->value->uint8;
        }
    }
    else if(tuple->key == NETDL_END){
        if (ctx->data && ctx->length > 0 && ctx->index > 0) {
            NetDownload *image = malloc(sizeof(NetDownload));
            image->data = ctx->data;
            image->length = ctx->length;

            //printf("Received file of size=%lu and address=%p", ctx->length, ctx->data);
            ctx->callback(image, ctx->right_half);

            ctx->data = NULL;
            ctx->index = ctx->length = 0;
        }
    }
}

