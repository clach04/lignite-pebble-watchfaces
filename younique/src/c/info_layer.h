#pragma once
#ifdef PBL_ROUND
#define MAX_AMOUNT_OF_INFO_MODULES 3
#define BATTERY_BAR_HEIGHT 16
#else
#define MAX_AMOUNT_OF_INFO_MODULES 6
#define BATTERY_BAR_HEIGHT 6
#endif
#define INFO_LAYER_MODULE_STORAGE_KEY 69
#define MODULE_HEIGHT 30
#define MODULE_SIDE_SPACING   4
#define MODULE_BOTTOM_SPACING 6
#define INFO_MODULE_STORAGE_BASE_KEY 1000

typedef enum {
    InfoModuleTypeIconAndText = 0,
    InfoModuleTypeTitle
} InfoModuleType;

typedef enum {
    InfoModuleDataTypeBluetoothStatus = 0,
    InfoModuleDataTypeBatteryLevel,
    InfoModuleDataTypeWeather,
    InfoModuleDataTypeHealth
} InfoModuleDataType;

typedef enum {
    WeatherDataTypeCondition = 0,
    WeatherDataTypeTemperature
} WeatherDataType;

typedef struct {
    bool connected;
} InfoModuleDataBluetooth;

typedef struct {
    BatteryChargeState charge_state;
} InfoModuleDataBattery;

typedef struct {
    HealthMetric metric;
    uint32_t value;
} InfoModuleDataHealth;

typedef struct {
    InfoModuleDataType data_type;
    InfoModuleType type;
    bool enabled;
    uint8_t index;
    uint8_t temporary_sub_data;
} InfoModuleStorageData;

typedef struct {
    InfoModuleStorageData storage_data;
    char text[1][15];
    uint32_t icon_resource_id;
    GBitmap *icon;
    GSize icon_size;
    void *data;
} InfoModule;

typedef void (*InfoLayerUpdateCallback)();

uint8_t info_layer_get_amount_of_modules();
void info_layer_save_modules();
void info_layer_load_modules();
void info_layer_parse_module_dictionary(DictionaryIterator *iter);
void info_layer_set_other_text(char *other_text);
GSize info_layer_get_size(bool adjusted);
Layer *info_layer_get_layer();
void info_layer_bluetooth_handler(bool connected);
void info_layer_create(GRect frame, InfoLayerUpdateCallback callback);
void info_layer_destroy();

void health_reload_everything();