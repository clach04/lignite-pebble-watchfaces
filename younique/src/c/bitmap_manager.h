#pragma once
#include "info_layer.h"

#define AMOUNT_OF_BITMAPS 17

void bitmap_manager_assign_bitmap(InfoModule *module);
#ifdef PBL_COLOR
void bitmap_manager_reload_bitmaps(Settings new_settings);
#endif