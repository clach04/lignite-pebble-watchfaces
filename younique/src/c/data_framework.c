#include <pebble.h>
#include "lignite.h"
#include "data_framework.h"
#include "netdownload.h"
#include "info_layer.h"

//#define DEBUG_DEFAULT_SETTINGS

void settings_setup();
void settings_finish();
Settings data_framework_get_default_settings();

Settings data_framework_local_settings;
SettingsChangeCallback settings_callbacks[AMOUNT_OF_SETTINGS_CALLBACKS];

uint32_t data_framework_checksum = 0;
bool userWas1x, doNotVibrate; //If the user used this face before 2.0

bool parsed_module_dictionary = false;
bool call_settings_callback = true;

void data_framework_setup(){
    app_message_register_inbox_received(data_framework_inbox);
    app_message_register_inbox_dropped(data_framework_inbox_dropped);
    app_message_open(APP_MESSAGE_SIZE, 64);

    settings_setup();
}

void data_framework_finish(){
    settings_finish();
}

void process_tuple(Tuple *t, DictionaryIterator *iter, void *context){
    uint32_t key = t->key;
    int value = t->value->int32;

    //NSLog("Got key %lu with value %d", key, value);

    if(key == NETDL_DATA
            || key == NETDL_BEGIN
            || key == NETDL_IS_RIGHT_HALF
            || key == NETDL_END){
        doNotVibrate = true;
        netdownload_handle_new_data(t, iter, context);
        call_settings_callback = false;
    }
    else if(key == MESSAGE_KEY_infoModuleDataType
            || key == MESSAGE_KEY_infoModuleDisplayType
            || key == MESSAGE_KEY_infoModuleDataInfo
            || key == MESSAGE_KEY_infoModuleEnabled
            || key == MESSAGE_KEY_infoModuleIndex){
        if(parsed_module_dictionary){
            return;
        }

        info_layer_parse_module_dictionary(iter);

        parsed_module_dictionary = true;
        doNotVibrate = true;
    }
    else if(key == MESSAGE_KEY_condition){
        doNotVibrate = true;
        data_framework_local_settings.weather.condition = value;
    }
    else if(key == MESSAGE_KEY_temperature){
        data_framework_local_settings.weather.temperature =  value;
    }
    else if(key == MESSAGE_KEY_btdisalert){
        data_framework_local_settings.btdisalert = value;
    }
    else if(key == MESSAGE_KEY_younique_shake_mode){
        data_framework_local_settings.shake_mode = value;
    }
    else if(key == MESSAGE_KEY_night_mode){
        data_framework_local_settings.night_mode = value;
    }
    else if(key == MESSAGE_KEY_show_island_image){
        data_framework_local_settings.show_island_image = value;
    }
    else if(key == MESSAGE_KEY_large_time_font){
        data_framework_local_settings.large_time_font = value;
    }
    else if(key == MESSAGE_KEY_imperial_measurements){
        data_framework_local_settings.imperial_measurements = value;
    }
    #ifdef PBL_COLOR
    else if(key == MESSAGE_KEY_background_colour){
        data_framework_local_settings.background_colour = GColorFromHEX(data_framework_hex_string_to_uint(t->value->cstring));
    }
    else if(key == MESSAGE_KEY_module_colour){
        data_framework_local_settings.module_colour = GColorFromHEX(data_framework_hex_string_to_uint(t->value->cstring));
    }
    else if(key == MESSAGE_KEY_battery_bar_colour){
        data_framework_local_settings.battery_bar_colour = GColorFromHEX(data_framework_hex_string_to_uint(t->value->cstring));
    }
    #endif
    else if(key == MESSAGE_KEY_photo_timeout){
        const uint32_t photo_timeout_values[] = {
            666, 900000, 1800000, 3600000, 10800000
        };

        data_framework_local_settings.photo_timeout = photo_timeout_values[value];
        NSLog("Photo timeout set to %ld", data_framework_local_settings.photo_timeout);
    }
}

void data_framework_inbox(DictionaryIterator *iter, void *context){
    call_settings_callback = true;

    Tuple *t = dict_read_first(iter);
    if(t){
        process_tuple(t, iter, context);
    }
    while(t != NULL){
        t = dict_read_next(iter);
        if(t){
            process_tuple(t, iter, context);
        }
    }

    for(int i = 0; i < AMOUNT_OF_SETTINGS_CALLBACKS; i++){
        if(call_settings_callback){
            settings_callbacks[i](data_framework_local_settings);
        }
    }
    if(!doNotVibrate){
        vibes_double_pulse();
    }
    doNotVibrate = false;
    parsed_module_dictionary = false;
}

void data_framework_inbox_dropped(AppMessageResult reason, void *context){
    //NSLog("Message dropped, reason %d.", reason);
}

void data_framework_register_settings_callback(SettingsChangeCallback callback, SettingsCallback callbackIdentity){
    settings_callbacks[callbackIdentity] = callback;
}

//Cheers to http://forums.getpebble.com/discussion/comment/132931#Comment_132931
unsigned int data_framework_hex_string_to_uint(char const* hexstring){
    unsigned int result = 0;
    char const *c = hexstring;
    unsigned char thisC;
    while((thisC = *c) != 0){
        thisC = toupper(thisC);
        result <<= 4;
        if(isdigit(thisC)){
            result += thisC - '0';
        }
        else if(isxdigit(thisC)){
            result += thisC - 'A' + 10;
        }
        else{
            APP_LOG(APP_LOG_LEVEL_DEBUG, "ERROR: Unrecognised hex character \"%c\" for string %s", thisC, hexstring);
            return 0;
        }
        ++c;
    }
    return result;
}

Settings data_framework_get_settings(){
    #ifdef DEBUG_DEFAULT_SETTINGS
    return data_framework_get_default_settings();
    #else
    return data_framework_local_settings;
    #endif
}

Settings data_framework_get_default_settings(){
    Settings defaults;

    defaults.btdisalert = true;
    defaults.btrealert = false;

    defaults.night_mode = true;
    defaults.show_island_image = true;
    defaults.imperial_measurements = false;

    defaults.shake_mode = ShakeModeShowEverything;
    defaults.large_time_font = false;

    defaults.background_colour = GColorBlack;
    defaults.module_colour = GColorWhite;
    defaults.battery_bar_colour = GColorRed;

    defaults.photo_timeout = 1800000;

    strcpy(defaults.dateFormat[0], "%A, %d/%m/%y");

    return defaults;
}

#define CURRENT_STORAGE_VERSION 1

void data_framework_save_settings(bool logResult){
    persist_write_data(SETTINGS_KEY, &data_framework_local_settings, sizeof(data_framework_local_settings));
    persist_write_int(STORAGE_VERSION_KEY, CURRENT_STORAGE_VERSION);
}

int data_framework_load_settings(bool logResult){
    int result = persist_read_data(SETTINGS_KEY, &data_framework_local_settings, sizeof(data_framework_local_settings));
    if(result < 0){
        data_framework_local_settings = data_framework_get_default_settings();
    }
    return result;
}

void settings_setup() {
    uint8_t version = persist_read_int(STORAGE_VERSION_KEY);

    int result = data_framework_load_settings(LOG_ALL);

    //User was already using the watchface and then upgraded to 2.0
    if(result > 0 && version == 0){
        persist_write_bool(USER_WAS_1X_KEY, true);
        userWas1x = true;
        return;
    }
    //or, the user had a good version in place
    if(version >= CURRENT_STORAGE_VERSION){
        userWas1x = persist_read_bool(USER_WAS_1X_KEY);
        return;
    }

    data_framework_local_settings = data_framework_get_default_settings();
}

void settings_finish(){
    data_framework_save_settings(LOG_ALL);
}
