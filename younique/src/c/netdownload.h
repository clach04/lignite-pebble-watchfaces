#include <pebble.h>

#ifdef PBL_PLATFORM_APLITE
#define APP_MESSAGE_SIZE 510
#elif PBL_PLATFORM_DIORITE
#define APP_MESSAGE_SIZE 2001
#else
#define APP_MESSAGE_SIZE 1000
#endif

#define NETDL_BEGIN MESSAGE_KEY_photoTransferBegin
#define NETDL_IS_RIGHT_HALF MESSAGE_KEY_photoTransferIsRightHalf
#define NETDL_DATA MESSAGE_KEY_photoTransferData
#define NETDL_END MESSAGE_KEY_photoTransferEnd
#define NETDL_CHUNK_SIZE MESSAGE_KEY_photoTransferChunkSize

typedef struct {
    uint8_t*  data;
    uint32_t length;
} NetDownload;

typedef void (*NetDownloadCallback)(NetDownload *image, bool rightHalf);

typedef struct {
    bool right_half;
    uint32_t length;
    uint8_t *data;
    uint32_t index;
    NetDownloadCallback callback;
} NetDownloadContext;

NetDownloadContext* netdownload_create_context(NetDownloadCallback callback);

void netdownload_initialize();
void netdownload_deinitialize();

void netdownload_request(char *url);

void netdownload_destroy(NetDownload *image);

void netdownload_handle_new_data(Tuple *tuple, DictionaryIterator *iter, void *context);
void netdownload_dropped(AppMessageResult reason, void *context);
void netdownload_out_success(DictionaryIterator *iter, void *context);
void netdownload_out_failed(DictionaryIterator *iter, AppMessageResult reason, void *context);