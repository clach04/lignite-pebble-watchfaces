#include <pebble.h>
#include "lignite.h"
#include "info_layer.h"
#include "bitmap_manager.h"

void info_layer_hide();

Layer *info_layer;
InfoModule *modules[MAX_AMOUNT_OF_INFO_MODULES];
uint8_t amount_of_modules = 0;

static char info_date[30];
static char other_text_to_draw[30];
bool info_layer_draw_other_text = false;

BatteryChargeState info_charge_state;
Settings info_settings;

uint32_t info_health_data[9];

InfoModuleStorageData building_module;

GFont condition_font;

int percent_size_to_return = 0;
bool info_layer_hidden = true, info_layer_animating = false, info_animate_lock = true;
AppTimer *info_layer_hide_timer;

InfoLayerUpdateCallback info_layer_update_callback;

static const char *climacon_condition_codes[14] = {
	"B", //CLEAR_DAY
	"I", //CLEAR_NIGHT
	"Z", //WINDY
	"O", //COLD
	"C", //PARTLY_CLOUDY_DAY
	"J", //PARTLY_CLOUDY_NIGHT
	"N", //HAZE
	"P", //CLOUD
	"Q", //RAIN
	"W", //SNOW
	"X", //HAIL
	"P", //CLOUDY
	"V", //STORM
	"P", //NA
};

uint8_t info_layer_get_amount_of_modules(){
	return amount_of_modules;
}

void info_module_print_weather_data(InfoModule *module){
	if(module->storage_data.temporary_sub_data == WeatherDataTypeTemperature){
		snprintf(module->text[0], sizeof(module->text[0]), "%d°", info_settings.weather.temperature);
	}
	else{
		snprintf(module->text[0], sizeof(module->text[0]), "%s", climacon_condition_codes[info_settings.weather.condition]);
	}
}

void info_layer_settings_callback(Settings new_settings){
	#ifdef PBL_HEALTH
	bool reload_health = false;
	if(info_settings.imperial_measurements != new_settings.imperial_measurements){
		reload_health = true;
	}
	#endif
	info_settings = new_settings;

	#ifdef PBL_COLOR
	bitmap_manager_reload_bitmaps(new_settings);
	#endif
	for(int i = 0; i < MAX_AMOUNT_OF_INFO_MODULES; i++){
		if(modules[i]->storage_data.data_type == InfoModuleDataTypeWeather){
			info_module_print_weather_data(modules[i]);
		}
	}

	#ifdef PBL_HEALTH
	if(reload_health){
		health_reload_everything();
	}
	#endif

	layer_mark_dirty(info_layer);

	if(info_layer_hidden && !info_animate_lock){
		if(info_layer_hide_timer){
			app_timer_cancel(info_layer_hide_timer);
			info_layer_hide_timer = NULL;
		}
		info_layer_hide();
	}
	else if(info_layer_hidden && info_settings.shake_mode == ShakeModeDisabled && info_animate_lock){
		info_layer_hide();
	}
}

InfoModule *info_layer_get_module(uint8_t index){
	return modules[index];
}

void info_layer_save_modules(){
	for(int i = 0; i < MAX_AMOUNT_OF_INFO_MODULES; i++){
		persist_write_data(i+INFO_MODULE_STORAGE_BASE_KEY, &modules[i]->storage_data, sizeof(modules[i]->storage_data));
	}
}

InfoModule* info_module_create(){
	InfoModule *module = malloc(sizeof(InfoModule));

	module->icon_resource_id = 0;
	module->data = NULL;

	return module;
}

void info_module_load_data(InfoModule *module){
	if(module->data){
		free(module->data);
		module->data = NULL;
	}
	switch(module->storage_data.data_type){
		case InfoModuleDataTypeBluetoothStatus:{
			module->data = malloc(sizeof(InfoModuleDataBluetooth));
			break;
		}
		case InfoModuleDataTypeBatteryLevel:{
			module->data = malloc(sizeof(InfoModuleDataBattery));
			break;
		}
		#ifdef PBL_HEALTH
		case InfoModuleDataTypeHealth:{
			InfoModuleDataHealth *health_data = malloc(sizeof(InfoModuleDataHealth));
			health_data->metric = module->storage_data.temporary_sub_data;
			module->data = health_data;
			snprintf(module->text[0], sizeof(module->text[0]), "...");
			break;
		}
		#endif
		default:
			//NSWarn("Module at index %d has no assigned data structure.", module->storage_data.index);
			break;
	}
}

void info_layer_load_modules(){
	for(int i = 0; i < MAX_AMOUNT_OF_INFO_MODULES; i++){
		if(!modules[i]){
			modules[i] = info_module_create();
		}
		persist_read_data(i+INFO_MODULE_STORAGE_BASE_KEY, &modules[i]->storage_data, sizeof(modules[i]->storage_data));
	}
}

void graphics_animation_roll_started(Animation *animation, void *context){}
void graphics_animation_roll_stopped(Animation *animation, bool finished, void *context){}
void graphics_animation_roll_setup(Animation *animation){}

void graphics_animation_roll_update(Animation *animation, const AnimationProgress distance_normalized){
    long percent = (distance_normalized*100)/ANIMATION_NORMALIZED_MAX;

    percent_size_to_return = info_layer_hidden ? (100-(int)percent) : (int)percent;

	info_layer_update_callback();
}

void graphics_animation_roll_teardown(Animation *animation){
    animation_destroy(animation);
    AnimationImplementation *implementation = (AnimationImplementation*)animation_get_implementation(animation);
    free(implementation);

	info_layer_animating = false;
}

void info_layer_hide(){
	if(info_layer_animating){
		return;
	}

	if(!info_layer_hidden && info_settings.shake_mode == ShakeModeDisabled){
		return;
	}

	info_layer_animating = true;
	info_layer_hidden = !info_layer_hidden;

	if(info_layer_hide_timer){
		app_timer_cancel(info_layer_hide_timer);
	}
	info_layer_hide_timer = NULL;
	if(!info_layer_hidden){
		info_layer_hide_timer = app_timer_register(10000, info_layer_hide, NULL);
	}

	Animation *animation = animation_create();
	animation_set_duration(animation, 400);
	animation_set_delay(animation, 30);
	AnimationImplementation *implementation = malloc(sizeof(AnimationImplementation));
	implementation->setup = graphics_animation_roll_setup;
	implementation->update = graphics_animation_roll_update;
	implementation->teardown = graphics_animation_roll_teardown;
	AnimationHandlers handlers = (AnimationHandlers){
		.started = graphics_animation_roll_started,
		.stopped = graphics_animation_roll_stopped
	};
	animation_set_handlers(animation, handlers, NULL);
	animation_set_implementation(animation, implementation);
	animation_schedule(animation);
}

void info_layer_tap_handler(AccelAxisType axis, int32_t direction){
	if(info_settings.shake_mode == ShakeModeDisabled){
		return;
	}
	info_layer_hide();
}

GSize info_layer_get_size(bool adjusted){
	GSize info_layer_size = GSize(WINDOW_FRAME.size.w, 0);

	info_layer_size.h += BATTERY_BAR_HEIGHT;

	//Date stuff
	#ifdef PBL_ROUND
	info_layer_size.h += 36;
	#else
	info_layer_size.h += 26;
	#endif

	if(info_settings.shake_mode != ShakeModeShowDateAndBattery){
		#ifdef PBL_ROUND
		uint8_t amount_of_rows = (amount_of_modules == 3) ? 2 : (amount_of_modules);
		#else
		uint8_t amount_of_rows = (amount_of_modules/2) + (amount_of_modules%2);
		#endif
		info_layer_size.h += (amount_of_rows)*(MODULE_HEIGHT+MODULE_BOTTOM_SPACING);

		#ifdef PBL_ROUND
		if(amount_of_rows > 0){
			info_layer_size.h -= 4;
		}
		#endif
	}

	if(adjusted){
		info_layer_size.h = (info_layer_size.h*percent_size_to_return)/100;
	}

	return info_layer_size;
}

void info_layer_set_other_text(char *other_text){
	if(other_text){
		snprintf(other_text_to_draw, sizeof(other_text_to_draw), "%s", other_text);
		info_layer_draw_other_text = true;
	}
	else{
		info_layer_draw_other_text = false;
	}
	layer_mark_dirty(info_layer);
}

void info_layer_master_proc(Layer *layer, GContext *ctx){
	GSize info_layer_size = info_layer_get_size(true);
	GSize info_layer_unmodified_size = info_layer_get_size(false);
	int animated_adjust = (info_layer_unmodified_size.h-info_layer_size.h);
	GRect layer_frame = layer_get_unobstructed_bounds(layer);

	GRect info_layer_frame = GRect(0, layer_frame.size.h-info_layer_size.h, info_layer_size.w, info_layer_size.h);
	graphics_context_set_fill_color(ctx, info_settings.background_colour);
	graphics_fill_rect(ctx, info_layer_frame, 0, GCornerNone);

	//Battery bar stuff
	#ifdef PBL_COLOR
	GColor battery_bar_background_colour = GColorDarkGray;
	if(gcolor_equal(info_settings.battery_bar_colour, battery_bar_background_colour)){
		battery_bar_background_colour = GColorBlack;
	}
	#endif
	#ifdef PBL_ROUND
	int max_width = 60;
	int x_offset = (layer_frame.size.w-max_width)/2;
	int battery_bar_width = (info_charge_state.charge_percent*max_width)/100;
	int actual_battery_bar_height = 6;
	graphics_context_set_fill_color(ctx, PBL_IF_COLOR_ELSE(battery_bar_background_colour, GColorBlack));
	graphics_fill_rect(ctx, GRect(x_offset, layer_frame.size.h-(actual_battery_bar_height*3)+animated_adjust, max_width, actual_battery_bar_height), 2, GCornersAll);
	graphics_context_set_fill_color(ctx, PBL_IF_COLOR_ELSE(info_settings.battery_bar_colour, GColorWhite));
	graphics_fill_rect(ctx, GRect(x_offset, layer_frame.size.h-(actual_battery_bar_height*3)+animated_adjust, battery_bar_width, actual_battery_bar_height), 2, GCornersAll);
	#else
	int battery_bar_width = (info_charge_state.charge_percent*layer_frame.size.w)/100;
	graphics_context_set_fill_color(ctx, PBL_IF_COLOR_ELSE(battery_bar_background_colour, GColorBlack));
	graphics_fill_rect(ctx, GRect(0, layer_frame.size.h-BATTERY_BAR_HEIGHT+animated_adjust, layer_frame.size.w, BATTERY_BAR_HEIGHT), 0, GCornerNone);
	graphics_context_set_fill_color(ctx, PBL_IF_COLOR_ELSE(info_settings.battery_bar_colour, GColorWhite));
	graphics_fill_rect(ctx, GRect(0, layer_frame.size.h-BATTERY_BAR_HEIGHT+animated_adjust, battery_bar_width, BATTERY_BAR_HEIGHT), 0, GCornerNone);
	#endif
	//End battery bar stuff

	struct tm *t;
  	time_t temp;
  	temp = time(NULL);
  	t = localtime(&temp);

	strftime(info_date, sizeof(info_date), info_settings.dateFormat[0], t);
	graphics_context_set_text_color(ctx, gcolor_legible_over(info_settings.background_colour));
	graphics_draw_text(ctx, info_layer_draw_other_text ? other_text_to_draw : info_date, fonts_get_system_font(FONT_KEY_GOTHIC_14), GRect(0, info_layer_frame.origin.y+4, info_layer_frame.size.w, 16), GTextOverflowModeWordWrap, GTextAlignmentCenter, NULL);

	if(info_settings.shake_mode == ShakeModeShowDateAndBattery){
		return;
	}

	GTextAlignment text_alignment = GTextAlignmentLeft;

	graphics_context_set_fill_color(ctx, info_settings.module_colour);
	graphics_context_set_text_color(ctx, gcolor_legible_over(info_settings.module_colour));
	for(uint8_t i = 1; i < (amount_of_modules+1); i++){
		// NSLog("Drawing for module %d for amount of modules %d", i-1, amount_of_modules);
		InfoModule *module = modules[i-1];
		bool is_left_side = i%2;
		uint8_t row = (i/2)+(i%2);
		int module_width = (layer_frame.size.w/2) - (MODULE_SIDE_SPACING*2);
		GRect container_frame =
			GRect(is_left_side ? MODULE_SIDE_SPACING : (module_width+(MODULE_SIDE_SPACING*3)),
				  layer_frame.size.h-(row*(MODULE_BOTTOM_SPACING+MODULE_HEIGHT))-(BATTERY_BAR_HEIGHT)+animated_adjust,
				  module_width,
				  MODULE_HEIGHT);
		#ifdef PBL_ROUND
		bool is_whole_row = false;
		if(i == 1 && amount_of_modules != 2){
			container_frame.origin.x = ((layer_frame.size.w)/4)+4;
		}
		else{
			int tempRow = row+1;
			is_left_side = true;
			if(i == 3){
				tempRow = 2;
				is_left_side = false;
			}
			if(i == 2 && amount_of_modules == i){
				is_left_side = false;
			}
			module_width -= 12;
			container_frame = GRect(is_left_side ? MODULE_SIDE_SPACING : (module_width+(MODULE_SIDE_SPACING*3)),
				  layer_frame.size.h-(tempRow*(MODULE_BOTTOM_SPACING+MODULE_HEIGHT))-(BATTERY_BAR_HEIGHT)+animated_adjust,
				  module_width,
				  MODULE_HEIGHT);
			container_frame.origin.x += 12;
		}
		container_frame.origin.y -= 6;
		#endif
		GRect drawing_frame = container_frame;
		#ifndef PBL_ROUND
		bool is_whole_row = (i == amount_of_modules && is_left_side);
		if(is_whole_row){
			drawing_frame.size.w = (container_frame.size.w*2)+(MODULE_SIDE_SPACING*2);
			container_frame.origin.x += drawing_frame.size.w/4;
		}
		#endif
		graphics_fill_rect(ctx, drawing_frame, 2, GCornersAll);
		switch(module->storage_data.type){
			case InfoModuleTypeIconAndText:{
				int container_frame_center = (container_frame.origin.y+(container_frame.size.h/2));
				GRect icon_frame =
					GRect(container_frame.origin.x,
						container_frame_center-(module->icon_size.h/2),
						module->icon_size.w,
						module->icon_size.h);

				uint8_t text_size_pt = 18;
				GRect text_frame =
					GRect(icon_frame.origin.x+icon_frame.size.w+4,
						container_frame_center-(text_size_pt/2),
						0,
						text_size_pt);
				text_frame.size.w = (is_whole_row ? drawing_frame.size.w : container_frame.size.w)-icon_frame.size.w-8;

				GFont text_font = fonts_get_system_font(FONT_KEY_GOTHIC_18);

				bool small_text_adjust = false;
				if(module->storage_data.data_type == InfoModuleDataTypeBluetoothStatus){
					text_font = fonts_get_system_font(FONT_KEY_GOTHIC_14);
					small_text_adjust = true;
				}

				GSize text_size = graphics_text_layout_get_content_size(module->text[0], text_font, text_frame, GTextOverflowModeWordWrap, text_alignment);

				if(text_size.h > 20 && !is_whole_row){
					text_font = fonts_get_system_font(FONT_KEY_GOTHIC_14);
					small_text_adjust = true;
					text_size = graphics_text_layout_get_content_size(module->text[0], text_font, text_frame, GTextOverflowModeWordWrap, text_alignment);
				}

				text_frame = GRect(text_frame.origin.x,
								container_frame_center-(text_size.h/2)-4 + small_text_adjust,
								text_size.w,
								text_size.h);

				//NSLog("Drawing '%s' to GRect(%d, %d, %d, %d) with an actual size of GSize(%d, %d)", module->text[0], text_frame.origin.x, text_frame.origin.y, text_frame.size.w, text_frame.size.h, text_size.w, text_size.h);

				int center_adjust = (container_frame.size.w-((text_frame.origin.x-container_frame.origin.x)+text_frame.size.w))/2;

				icon_frame.origin.x += center_adjust;
				text_frame.origin.x += center_adjust;

				graphics_context_set_compositing_mode(ctx, GCompOpSet);
				graphics_draw_bitmap_in_rect(ctx, module->icon, icon_frame);

				graphics_draw_text(ctx, module->text[0], text_font, text_frame, GTextOverflowModeWordWrap, text_alignment, NULL);
				break;
			}
			case InfoModuleTypeTitle:{
				GFont title_font = fonts_get_system_font(FONT_KEY_GOTHIC_24);
				uint8_t adjust = 4;
				if(module->storage_data.data_type == InfoModuleDataTypeWeather && module->storage_data.temporary_sub_data == WeatherDataTypeCondition){
					title_font = condition_font;
					adjust = 4;
				}

				GSize title_size = graphics_text_layout_get_content_size(module->text[0], title_font, container_frame, GTextOverflowModeWordWrap, text_alignment);
				GRect title_frame =
					GRect(container_frame.origin.x+(container_frame.size.w/2)-(title_size.w/2), container_frame.origin.y+(container_frame.size.h/2)-(title_size.h/2)-adjust, title_size.w, title_size.h);

				graphics_draw_text(ctx, module->text[0], title_font, title_frame, GTextOverflowModeWordWrap, text_alignment, NULL);
				break;
			}
		}
	}
}

void info_layer_battery_handler(BatteryChargeState state){
	bool redraw = false;
	if(info_charge_state.charge_percent != state.charge_percent){
		redraw = true;
	}
	info_charge_state = state;

	for(uint8_t i = 0; i < amount_of_modules; i++){
		InfoModule *module = modules[i];
		if(module->storage_data.data_type == InfoModuleDataTypeBatteryLevel){
			redraw = true;

			InfoModuleDataBattery *battery_data = (InfoModuleDataBattery*)module->data;
			battery_data->charge_state = state;
			snprintf(module->text[0], sizeof(module->text[0]), "%d%%", state.charge_percent);

			bitmap_manager_assign_bitmap(module);
		}
	}

	if(redraw){
		layer_mark_dirty(info_layer);
	}
}

void info_layer_bluetooth_handler(bool connected){
	bool redraw = false;
	for(uint8_t i = 0; i < amount_of_modules; i++){
		InfoModule *module = modules[i];
		if(module->storage_data.data_type == InfoModuleDataTypeBluetoothStatus){
			redraw = true;

			InfoModuleDataBluetooth *bluetooth_data = (InfoModuleDataBluetooth*)module->data;
			bluetooth_data->connected = connected;
			snprintf(module->text[0], sizeof(module->text[0]), connected ? "Paired" : "Not paired");

			bitmap_manager_assign_bitmap(module);
		}
	}

	if(redraw){
		layer_mark_dirty(info_layer);
	}
}

#ifdef PBL_HEALTH
void health_format_value(InfoModule *module){
	InfoModuleDataHealth *health_data = (InfoModuleDataHealth*)module->data;

    uint32_t thousands = 0, hundreds = 0;

    switch(health_data->metric){
        case HealthMetricStepCount:
            if(health_data->value > 9999){
                thousands = health_data->value/1000;
                hundreds = (health_data->value % 1000)/100;
                snprintf(module->text[0], sizeof(module->text[0]), "%ld.%ldk", thousands, hundreds);
            }
            else{
                snprintf(module->text[0], sizeof(module->text[0]), "%ld", health_data->value);
            }
            break;
        case HealthMetricSleepSeconds:
        case HealthMetricSleepRestfulSeconds:
        case HealthMetricActiveSeconds:
		//APP_LOG(APP_LOG_LEVEL_INFO, "%ld", health_data->value);
            if(health_data->value < 60){
                snprintf(module->text[0], sizeof(module->text[0]), "%lds", health_data->value);
            }
            else if(health_data->value > 59 && health_data->value < 3600){
                thousands = health_data->value/60;
                hundreds = (health_data->value % 60);
				if(hundreds > 0){
                	snprintf(module->text[0], sizeof(module->text[0]), "%ldm %lds", thousands, hundreds);
				}
				else{
					snprintf(module->text[0], sizeof(module->text[0]), "%ldm", thousands);
				}
            }
            else{
                thousands = health_data->value/3600;
                hundreds = (health_data->value % 3600)/60;
                snprintf(module->text[0], sizeof(module->text[0]), "%ldh %ldm", thousands, hundreds);
            }
            break;
        case HealthMetricWalkedDistanceMeters:
			NSLog("%d", health_service_get_measurement_system_for_display(health_data->metric));
			if(info_settings.imperial_measurements){
				uint32_t freak_value = (health_data->value*33)/10;
				if(freak_value < 528){
	                snprintf(module->text[0], sizeof(module->text[0]), "%ldft", freak_value);
	            }
	            else{
	                thousands = freak_value/5280;
	                hundreds = (freak_value % 5280)/528;
	                snprintf(module->text[0], sizeof(module->text[0]), "%ld.%ldmi", thousands, hundreds);
	            }
			}
			else{
	            if(health_data->value <= 999){
	                snprintf(module->text[0], sizeof(module->text[0]), "%ldm", health_data->value);
	            }
	            else{
	                thousands = health_data->value/1000;
	                hundreds = (health_data->value % 1000)/100;
	                snprintf(module->text[0], sizeof(module->text[0]), "%ld.%ldkm", thousands, hundreds);
	            }
			}
            break;
		case HealthMetricActiveKCalories:
		case HealthMetricRestingKCalories:
			snprintf(module->text[0], sizeof(module->text[0]), "%ld", health_data->value);
			break;
		case HealthMetricHeartRateBPM:
			snprintf(module->text[0], sizeof(module->text[0]), "%ld BPM", health_data->value);
			break;
		default:
			break;
    }
    //NSLog(module->text[0]);
}

void health_update_data_for_metric(HealthMetric metric){
	time_t end = 0;
	time_t start = 0;

	HealthServiceAccessibilityMask mask;
	if(metric == HealthMetricHeartRateBPM){
		mask = HealthServiceAccessibilityMaskAvailable;
	}
	else{
		end = time(NULL);
		start = time_start_of_today();
		mask = health_service_metric_accessible(metric, start, end);
	}
	if(mask == HealthServiceAccessibilityMaskAvailable || metric == HealthMetricHeartRateBPM) {
		if(metric != HealthMetricHeartRateBPM){
			info_health_data[metric] = health_service_sum_today(metric);
		}
		else{
			info_health_data[metric] = health_service_peek_current_value(HealthMetricHeartRateBPM);
		}
		for(uint8_t i = 0; i < amount_of_modules; i++){
			if(modules[i]->storage_data.data_type == InfoModuleDataTypeHealth){
				InfoModuleDataHealth *health_data = (InfoModuleDataHealth*)modules[i]->data;
				if(health_data->metric == metric){
					health_data->value = info_health_data[metric];
					health_format_value(modules[i]);
				}
			}
		}
	}
	else {
		//APP_LOG(APP_LOG_LEVEL_ERROR, "Data unavailable!");
	}
}

void health_reload_sleep(){
	health_update_data_for_metric(HealthMetricSleepSeconds);
	health_update_data_for_metric(HealthMetricSleepRestfulSeconds);
}

void health_reload_movement(){
	health_update_data_for_metric(HealthMetricStepCount);
	health_update_data_for_metric(HealthMetricActiveSeconds);
	health_update_data_for_metric(HealthMetricWalkedDistanceMeters);
	health_update_data_for_metric(HealthMetricActiveKCalories);
	health_update_data_for_metric(HealthMetricRestingKCalories);
	health_update_data_for_metric(HealthMetricHeartRateBPM);
}

void health_reload_everything(){
	health_reload_sleep();
	health_reload_movement();
}

void health_handler(HealthEventType event, void *context) {
	switch(event) {
		case HealthEventSignificantUpdate:
			health_reload_everything();
			break;
		case HealthEventMovementUpdate:
			health_reload_movement();
			break;
		case HealthEventSleepUpdate:
			health_reload_sleep();
			break;
		case HealthEventMetricAlert:
			//What
			break;
		case HealthEventHeartRateUpdate:
			health_update_data_for_metric(HealthMetricHeartRateBPM);
			break;
	}
}
#endif

void info_layer_process_tuple(Tuple *t){
    uint32_t key = t->key;
	uint16_t value = t->value->uint16;

	//NSDebug("Processing key %d with value %d.", (int)key, value);

    if(key == MESSAGE_KEY_infoModuleDisplayType){
		building_module.type = value;
	}
	else if(key == MESSAGE_KEY_infoModuleDataType){
		building_module.data_type = value;
	}
	else if(key == MESSAGE_KEY_infoModuleIndex){
		building_module.index = value;
	}
	else if(key == MESSAGE_KEY_infoModuleEnabled){
		building_module.enabled = value;
	}
	else if(key == MESSAGE_KEY_infoModuleDataInfo){
		building_module.temporary_sub_data = value;
	}
}

void info_module_reload(InfoModule *module){
	info_module_load_data(module);

	if(module->storage_data.data_type == InfoModuleDataTypeBluetoothStatus){
		info_layer_bluetooth_handler(connection_service_peek_pebble_app_connection());
	}
	else if(module->storage_data.data_type == InfoModuleDataTypeBatteryLevel){
		info_layer_battery_handler(battery_state_service_peek());
	}
	else if(module->storage_data.data_type == InfoModuleDataTypeWeather){
		info_module_print_weather_data(module);
	}
	#ifdef PBL_HEALTH
	else if(module->storage_data.data_type == InfoModuleDataTypeHealth){
		health_reload_everything();
		bitmap_manager_assign_bitmap(module);
	}
	#endif
}

void info_layer_remove_module(int index){
   for(int i = index; i < MAX_AMOUNT_OF_INFO_MODULES - 1; i++){
	   modules[i]->storage_data = modules[i + 1]->storage_data;
	   info_module_reload(modules[i]);
   }
}

void info_layer_parse_module_dictionary(DictionaryIterator *iter){
	Tuple *t = dict_read_first(iter);
    if(t){
        info_layer_process_tuple(t);
    }
    while(t != NULL){
        t = dict_read_next(iter);
        if(t){
            info_layer_process_tuple(t);
        }
    }

	if(!building_module.enabled && !modules[building_module.index]->storage_data.enabled){
		return;
	}

	if(building_module.enabled){
		if(building_module.index == amount_of_modules){
			if(amount_of_modules == MAX_AMOUNT_OF_INFO_MODULES){
				return;
			}
			amount_of_modules++;
		}

		memcpy(&modules[building_module.index]->storage_data, &building_module, sizeof(building_module));

		info_module_reload(modules[building_module.index]);
	}
	else{
		NSWarn("Removing module at %d", building_module.index);
		info_layer_remove_module(building_module.index);
		amount_of_modules--;
	}

	layer_mark_dirty(info_layer);
}

Layer *info_layer_get_layer(){
	return info_layer;
}

void disable_animate_lock(){
	info_animate_lock = false;
}

void info_layer_create(GRect frame, InfoLayerUpdateCallback callback){
	info_layer_update_callback = callback;

	condition_font = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_WEATHER_ICONS_30));

	for(int i = 0; i < MAX_AMOUNT_OF_INFO_MODULES; i++){
		info_module_load_data(modules[i]);

		if(modules[i]->storage_data.enabled){
			amount_of_modules++;
		}
		if(modules[i]->storage_data.data_type == InfoModuleDataTypeHealth){
			bitmap_manager_assign_bitmap(modules[i]);
		}
		else if(modules[i]->storage_data.data_type == InfoModuleDataTypeWeather){
			info_module_print_weather_data(modules[i]);
		}
	}

	info_layer = layer_create(frame);
	layer_set_update_proc(info_layer, info_layer_master_proc);

	data_framework_register_settings_callback(info_layer_settings_callback, SETTINGS_CALLBACK_INFO_LAYER);
	info_layer_settings_callback(data_framework_get_settings());

	battery_state_service_subscribe(info_layer_battery_handler);
	info_layer_battery_handler(battery_state_service_peek());

	accel_tap_service_subscribe(info_layer_tap_handler);

	info_layer_bluetooth_handler(connection_service_peek_pebble_app_connection());

	app_timer_register(3000, disable_animate_lock, NULL);

	#ifdef PBL_HEALTH
	health_service_events_subscribe(health_handler, NULL);
	#endif
}

void info_layer_destroy(){
	layer_destroy(info_layer);
}