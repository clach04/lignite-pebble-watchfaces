#include <pebble.h>
#include "lignite.h"
#include "phrases.h"

Settings phrases_settings;

const SlotPack sober_slotpack = {
	.pack = {
		"Ohh", "Hell", "Blimey", "Flip", "Eek", "Oo-er"
	}
};

#ifdef PBL_ROUND
const BluetoothPack sober_btpack = {
	.disconnected = {
		"Disconnected", "Oops, BT off", "Darnit, off"
	},
	.connected = {
		"Connected :)", "Woo, BT on!", "BT working!"
	}
};

const BluetoothPack wasted_btpack = {
	.disconnected = {
		"Fuck's off", "BT's a bitch", "Fucking shit",
	},

	.connected = {
		"BT fuckin G", "BT on, bitch.", "Bitch on.",
	}
};
#else
const BluetoothPack sober_btpack = {
	.disconnected = {
		"Disconnected :(", "Oops, BT's off", "Darn thing's off"
	},
	.connected = {
		"Connected :)", "Hooray, connection!", "Lovely BT is working"
	}
};

const BluetoothPack wasted_btpack = {
	.disconnected = {
		"Fuck's fucked off", "Bastard BT's fucked", "Fuck shit bollocks",
	},

	.connected = {
		"BT's fucking working.", "She's online, bitch.", "Bitch connected.",
	}
};
#endif

const SlotPack wasted_slotpack = {
	.pack = {
		"Shit", "Whore", "Douche", "Cunt", "Fuck", "Mofo"
	}
};

const char sober_wpack[14][30] = {
	"It's lovely out", "Lovely night", "Rather windy", "A bit chilly", "Partly cloudy", "Partly cloudy night",
	"Some haze", "Pretty cloudy", "Raining rather hard", "Snow, you Canadian", "It's hailing", "Still cloudy",
	"Storming", "Shoot, error"
};

const char wasted_wpack[14][30] = {
	"Shit's fucking clear", "Clear ass night", "Windy shit", "Cold, you fuck", "Some fucking clouds", "Nightly damn clouds",
	"Hazy twat", "Shit's cloudy af", "Drizzle of fuck", "Fucking Canadian", "Hailing you dip", "CLOUDY AS FUCK", "Stormy cunt"
	"Fucking error"
};

static char wordBuffer[2][18] = {
	"freakin", "frikken"
};

WordPack sober_wordpack = {
	.pack = {
		"flippin", "frikkin", "darnit", "bangin", "truckin"
	}
};

WordPack wasted_wordpack = {
	.pack = {
		"fucking", "bitchin", "shittin", "fisting", "friggin"
	},
};

void phrases_set_settings(Settings settings){
	phrases_settings = settings;
}

char *phrases_get_main_swear(bool altBuffer, bool generateNew){
	int rNum = get_random_int(0, 0);
	snprintf(wordBuffer[altBuffer], sizeof(wordBuffer[altBuffer]), "%s", phrases_settings.soberMode ? sober_wordpack.pack[rNum] : wasted_wordpack.pack[rNum]);
	return wordBuffer[altBuffer];
}

const char *phrases_get_bluetooth(bool connected){
	int rNum = rand() % 3;
	if(phrases_settings.soberMode){
		if(connected){
			return sober_btpack.connected[rNum];
		}
		else{
			return sober_btpack.disconnected[rNum];
		}
	}
	else{
		if(connected){
			return wasted_btpack.connected[rNum];
		}
		else{
			return wasted_btpack.disconnected[rNum];
		}
	}
	return "e404pgb";
}

const char *phrases_get_slot_swear(){
	int rNum = rand() % 3;
	return phrases_settings.soberMode ? sober_slotpack.pack[rNum] : wasted_slotpack.pack[rNum];
}

const char *phrases_get_weather(int condition){
	return phrases_settings.soberMode ? sober_wpack[condition] : wasted_wpack[condition];
}
