#include <pebble.h>
#include "lignite.h"
#include "main_window.h"
#include "phrases.h"
#include "extra_window.h"

//#define TEST_LONG_WORDS

Window *main_window;
Layer *window_layer, *graphics_layer;
TextLayer *first_word_layer, *swear_word_layer, *third_word_layer, *fourth_word_layer, *slot_layer;
NewWord *newWords[4];
bool animating_layer[4];

int hour, minute, day, mday, second, fixedHour, teenStatus, bottomMin;
int padding = 6;
bool altWordBuffer, boot;
bool alreadyHiding = false;
char word1Buffer[10], word3Buffer[10], word4Buffer[10], slotBuffer[30];

#ifdef PBL_ROUND
int verticalShift = 14;
#else
int verticalShift = 0;
#endif

Settings settings;
BatteryChargeState current_charge_state;
Notification *disconnect_notification, *reconnect_notification;

char *ones[1][10] = {
	{
	  	"o'clock",
	  	"one",
	  	"two",
	  	"three",
	  	"four",
	  	"five",
	  	"six",
	  	"seven",
	  	"eight",
	  	"nine"
	}
};

char *teens[1][10] = {
	{
	  	"",
	  	"eleven",
	  	"twelve",
	  	"thirteen",
	  	"fourteen",
	  	"fifteen",
	  	"sixteen",
	  	"seven",
	  	"eight",
	  	"nine",
	}
};

char *tens[1][10] = {
	{
	  	"",
	  	"ten",
	  	"twenty",
	  	"thirty",
	  	"forty",
	  	"fifty",
	  	"sixty",
	  	"seventy",
	  	"eighty",
	  	"ninety"
	}
};

char *daysChar[1][7] = {
	{
		"Sunday.",
		"Monday.",
		"Tuesday.",
		"Wed.",
		"Thursday.",
		"Friday.",
		"Saturday."
	}
};

char *hourWords[1][12] = {
	{
		"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve"
	}
};

void update_slot_contents(int data){
	switch(settings.slotUse){
		case SlotUseBluetoothStatus:
			text_layer_set_text(slot_layer, phrases_get_bluetooth(data));
			break;
		case SlotUseTemperature:;
			static char temperatureBuffer[1][16];
			snprintf(temperatureBuffer[0], sizeof(temperatureBuffer[0]), "%s, %d°.", phrases_get_slot_swear(), settings.currentWeather.temperature);
			text_layer_set_text(slot_layer, temperatureBuffer[0]);
			break;
		case SlotUseBatteryLevel:;
			static char batteryBuffer[1][16];
			snprintf(batteryBuffer[0], sizeof(batteryBuffer[0]), "%s, %d%%.", phrases_get_slot_swear(), data);
			text_layer_set_text(slot_layer, batteryBuffer[0]);
			break;
		case SlotUseNothing:
			text_layer_set_text(slot_layer, "");
			break;
		default:
			//Already handled by something else...
			break;
	}
}

void on_word_animation_stopped(Animation *anim, bool finished, void *context){
	NewWord *newWord = (NewWord*)context;
	text_layer_set_text(newWord->layerToUpdate, newWord->word[newWord->alternate]);
	newWord->alternate = !newWord->alternate;
	GRect currentFrame = GRect(-150, newWord->originalFrame.origin.y, newWord->originalFrame.size.w, newWord->originalFrame.size.h);

	//NSLog("Submitting final animation for layer %p", newWord->layerToUpdate);

	animate_layer(text_layer_get_layer(newWord->layerToUpdate), &currentFrame, &newWord->originalFrame, 500, 500);

	int8_t layer_id = -1;
	if(newWord->layerToUpdate == first_word_layer){
		layer_id = 0;
		//NSLog("Setting %d to not animating", layer_id);
		animating_layer[layer_id] = false;
	}
	else if(newWord->layerToUpdate == swear_word_layer){
		layer_id = 1;
		//NSLog("Setting %d to not animating", layer_id);
		animating_layer[layer_id] = false;
	}
	else if(newWord->layerToUpdate == third_word_layer){
		layer_id = 2;
		//NSLog("Setting %d to not animating", layer_id);
		animating_layer[layer_id] = false;
	}
	else{
		layer_id = 3;
		//NSLog("Setting %d to not animating", layer_id);
		animating_layer[layer_id] = false;
	}

    #ifdef PBL_BW
        property_animation_destroy((PropertyAnimation*) anim);
    #endif
}

void animate_word_layer(TextLayer *layer, int delay, char *wordToChangeTo){
	int8_t layer_id = -1;
	if(layer == first_word_layer){
		layer_id = 0;
	}
	else if(layer == swear_word_layer){
		layer_id = 1;
	}
	else if(layer == third_word_layer){
		layer_id = 2;
	}
	else{
		layer_id = 3;
	}

	//NSLog("This is layer %d", layer_id);
	if(animating_layer[layer_id]){
		//NSLog("Rejecting layer %d since it's already animating", layer_id);
		return;
	}

	if(layer_id < 0){
		NSError("IDK this layer");
		return;
	}

	int length = 500;

	Layer *rawLayer = text_layer_get_layer(layer);
	GRect currentRect = layer_get_frame(rawLayer);

	GRect start = currentRect;
	GRect finish = GRect(300, currentRect.origin.y, currentRect.size.w, currentRect.size.h);

    PropertyAnimation *anim = property_animation_create_layer_frame(rawLayer, &start, &finish);

    animation_set_duration(property_animation_get_animation(anim), length);
    animation_set_delay(property_animation_get_animation(anim), delay);

	if(!newWords[layer_id]){
		newWords[layer_id] = malloc(sizeof(NewWord));
	}

	NewWord *newWord = newWords[layer_id];
	strncpy(newWord->word[newWord->alternate], wordToChangeTo, sizeof(newWord->word[newWord->alternate]));
	newWord->layerToUpdate = layer;
	newWord->originalFrame = start;

	//NSLog("Got original frame for layer %p: GRect(%d, %d, %d, %d)", layer, newWord->originalFrame.origin.x, newWord->originalFrame.origin.y, newWord->originalFrame.size.w, newWord->originalFrame.size.h);

	if(newWord->originalFrame.origin.x > 30 || newWord->originalFrame.origin.x < 0){
		//NSLog("Invalid frame! Rejecting request to animate.");
		return;
	}

	animating_layer[layer_id] = true;

	AnimationHandlers handlers = {
    	.stopped = (AnimationStoppedHandler) on_word_animation_stopped
    };
    animation_set_handlers(property_animation_get_animation(anim), handlers, newWord);

    animation_schedule(property_animation_get_animation(anim));
}

void update_swear_word(){
	//NSLog("Updating swear word %d", heap_bytes_free());

	phrases_get_main_swear(true, true);
	phrases_get_main_swear(false, true);
	animate_word_layer(swear_word_layer, 50, phrases_get_main_swear(altWordBuffer, false));
}

void tick_handler(struct tm *tick_time, TimeUnits units_changed){
	bool hourChanged, minuteChanged;
	#ifdef TEST_LONG_WORDS
	hourChanged = true;
	minuteChanged = true;

	hour = 11;
	minute = 34;
	day = tick_time->tm_wday;
	mday = tick_time->tm_mday;
	second = tick_time->tm_sec;
	#else
	hourChanged = (hour != tick_time->tm_hour);
	minuteChanged = (minute != tick_time->tm_min);

	hour = tick_time->tm_hour;
	minute = tick_time->tm_min;
	day = tick_time->tm_wday;
	mday = tick_time->tm_mday;
	second = tick_time->tm_sec;
	#endif

	if(!boot){
		hourChanged = true;
		minuteChanged = true;
		boot = true;
	}

	if(hour > 12){
		fixedHour = hour-13;
	}
	else if(hour != 0){
		fixedHour = hour-1;
	}
	else if(hour == 0){
		fixedHour = 11;
	}

	snprintf(word1Buffer, sizeof(word1Buffer), "%s", hourWords[0][fixedHour]);
	if(hourChanged){
		animate_word_layer(first_word_layer, 10, word1Buffer);
	}

	bottomMin = (minute%10);
	if(minute < 10){
		snprintf(word3Buffer, sizeof(word3Buffer), "%s", ones[0][bottomMin]);
		teenStatus = 1;
	}
	else if(minute > 10 && minute < 20){
		snprintf(word3Buffer, sizeof(word3Buffer), "%s", teens[0][(minute%10)]);
		if(minute < 17){
			teenStatus = 1;
		}
		else{
			teenStatus = 2;
		}
	}
	else{
		snprintf(word3Buffer, sizeof(word3Buffer), "%s", tens[0][(minute/10)]);
		teenStatus = 0;
	}

	if(minuteChanged){
		animate_word_layer(third_word_layer, 100, word3Buffer);
	}

	switch(teenStatus){
		case 0:
			snprintf(word4Buffer, sizeof(word4Buffer), "%s", ones[0][bottomMin]);
			if(layer_get_hidden(text_layer_get_layer(fourth_word_layer))){
				layer_set_hidden(text_layer_get_layer(fourth_word_layer), false);
			}
			if(minuteChanged){
				animate_word_layer(fourth_word_layer, 200, word4Buffer);
			}
			break;
		case 1:
			layer_set_hidden(text_layer_get_layer(fourth_word_layer), true);
			break;
		case 2:
			if(minuteChanged){
				animate_word_layer(fourth_word_layer, 200, "teen");
			}
			break;
	}

	if(minute % 10 == 0){
		if(layer_get_hidden(text_layer_get_layer(fourth_word_layer)) == 0){
			layer_set_hidden(text_layer_get_layer(fourth_word_layer), true);
		}
	}
	else{
		if(layer_get_hidden(text_layer_get_layer(fourth_word_layer)) && teenStatus != 1){
			layer_set_hidden(text_layer_get_layer(fourth_word_layer), false);
		}
	}

	switch(settings.slotUse){
		case SlotUseDate:;
			static char swearingDate[1][30];
			strftime(slotBuffer, sizeof(slotBuffer), settings.dateFormat[0], tick_time);
			snprintf(swearingDate[0], sizeof(swearingDate[0]), "%s, %s", phrases_get_slot_swear(), slotBuffer);
			text_layer_set_text(slot_layer, swearingDate[0]);
			break;
		case SlotUseDigitalTime:
			strftime(slotBuffer, sizeof(slotBuffer), clock_is_24h_style() ? "%H:%M" : "%I:%M", tick_time);
			text_layer_set_text(slot_layer, slotBuffer);
			break;
		default:
			//Handled by the main slot use handler
			break;
	}

	if(minute == 20){
		if(hour == 4 || hour == 16){
			text_layer_set_text(swear_word_layer, "blazeit");
		}
	}
}

void graphics_layer_proc(Layer *layer, GContext *ctx){
	#ifdef PBL_ROUND
	GRect foregroundFrame = grect_inset(layer_get_frame(graphics_layer), GEdgeInsets(4));
	graphics_context_set_fill_color(ctx, PBL_IF_COLOR_ELSE(settings.graphicsColour, GColorWhite));
	graphics_fill_radial(ctx, foregroundFrame, GOvalScaleModeFitCircle, 3, DEG_TO_TRIGANGLE(0), DEG_TO_TRIGANGLE(36*(current_charge_state.charge_percent/10)));
	#else
	int fix = (current_charge_state.charge_percent*(144-(padding*2)))/100;
	graphics_context_set_stroke_color(ctx, PBL_IF_COLOR_ELSE(settings.graphicsColour, GColorWhite));
	graphics_draw_line(ctx, GPoint(padding, 166), GPoint(fix, 166));
	graphics_draw_line(ctx, GPoint(padding, 167), GPoint(fix, 167));
	#endif
}

void battery_handler(BatteryChargeState state){
	update_slot_contents(state.charge_percent);
	current_charge_state = state;
	layer_mark_dirty(graphics_layer);
}

void bluetooth_handler(bool connected){
	if(connected && settings.btrealert){
		if(disconnect_notification->is_live){
			notification_force_dismiss(disconnect_notification);
		}
		notification_push(reconnect_notification, 10000);
		vibes_double_pulse();
	}
	else if(!connected && settings.btdisalert){
		if(reconnect_notification->is_live){
			notification_force_dismiss(reconnect_notification);
		}
		notification_push(disconnect_notification, 10000);
		vibes_long_pulse();
	}

	update_slot_contents(connected);
}

void mark_as_no_longer_hiding(){
	alreadyHiding = false;
}

void shake_handler(AccelAxisType axis, int32_t direction){
	//NSLog("Got a shake %d", settings.shakeUse);
	switch(settings.shakeUse){
		case ShakeUseChangeSwear:
			update_swear_word();
			break;
		case ShakeUseExtraScreen:
			if(!extra_window_is_loaded()){
				extra_window_update_settings(settings, tick_handler);
				window_stack_push(extra_window_get_window(), true);
			}
			else{
				window_stack_pop(true);
				tick_timer_service_subscribe(MINUTE_UNIT, tick_handler);
			}
			break;
		case ShakeUseHide:;
			if(alreadyHiding){
				return;
			}
			Layer *swearWordRawLayer = text_layer_get_layer(swear_word_layer);
			GRect currentSwearWordFrame = layer_get_frame(swearWordRawLayer);
			GRect newSwearWordFrame = GRect(currentSwearWordFrame.origin.x, -100, currentSwearWordFrame.size.w, currentSwearWordFrame.size.h);

			Layer *thirdWordRawLayer = text_layer_get_layer(third_word_layer);
			GRect currentThirdWordFrame = layer_get_frame(thirdWordRawLayer);

			Layer *fourthWordRawLayer = text_layer_get_layer(fourth_word_layer);
			GRect currentFourthWordFrame = layer_get_frame(fourthWordRawLayer);

			animate_layer(swearWordRawLayer, &currentSwearWordFrame, &newSwearWordFrame, 600, 10);
			animate_layer(swearWordRawLayer, &newSwearWordFrame, &currentSwearWordFrame, 600, 5000);

			animate_layer(thirdWordRawLayer, &currentThirdWordFrame, &currentSwearWordFrame, 600, 10);
			animate_layer(thirdWordRawLayer, &currentSwearWordFrame, &currentThirdWordFrame, 600, 5000);

			animate_layer(fourthWordRawLayer, &currentFourthWordFrame, &currentThirdWordFrame, 600, 10);
			animate_layer(fourthWordRawLayer, &currentThirdWordFrame, &currentFourthWordFrame, 600, 5000);

			alreadyHiding = true;
			app_timer_register(6000, mark_as_no_longer_hiding, NULL);
			break;
		default:
			break;
	}
}

void fire_animation_callback(){
	//fire_animation(swear_word_layer, 0, get_new_frame(swear_word_layer, phrases_get_main_swear(!altWordBuffer, true)), true);
	altWordBuffer = !altWordBuffer;
}

void update_weather(){
	DictionaryIterator *iter;
	app_message_outbox_begin(&iter);

	app_timer_register(1800000, update_weather, NULL);

	if (iter == NULL) {
		APP_LOG(APP_LOG_LEVEL_ERROR, "Iter is NULL, refusing to send message.");
		return;
	}

	dict_write_uint16(iter, 200, 1);
	dict_write_end(iter);

	app_message_outbox_send();
}

void refresh_settings(){
	phrases_set_settings(settings);

	struct tm *t;
  	time_t temp;
  	temp = time(NULL);
  	t = localtime(&temp);
	tick_handler(t, 0);

	update_swear_word();

	text_layer_set_text_color(first_word_layer, settings.timeColour);
	text_layer_set_text_color(swear_word_layer, settings.swearColour);
	text_layer_set_text_color(third_word_layer, settings.timeColour);
	text_layer_set_text_color(fourth_word_layer, settings.timeColour);
	text_layer_set_text_color(slot_layer, settings.slotColour);
	window_set_background_color(main_window, settings.backgroundColour);

	battery_handler(battery_state_service_peek());

	layer_set_hidden(graphics_layer, !settings.batteryBar);

	if(settings.slotUse == SlotUseTemperature){
		update_slot_contents(settings.currentWeather.temperature);
	}

	if(settings.shakeUse == ShakeUseDisabled){
		accel_tap_service_unsubscribe();
	}
	else{
		accel_tap_service_unsubscribe();
		accel_tap_service_subscribe(shake_handler);
	}
}

void notification_push_handler(bool pushed, uint8_t id){
	if(pushed){
		layer_remove_from_parent(window_layer);
	}
	else{
		if(disconnect_notification->is_live || reconnect_notification->is_live){
			return;
		}
		layer_add_child(window_get_root_layer(main_window), window_layer);
	}
}

void main_window_settings_callback(Settings newSettings){
	settings = newSettings;
	refresh_settings();
}

void testing(){
	animate_word_layer(swear_word_layer, 10, "wo cao!");
}

void window_load_main(Window *window){
	Layer *window_root_layer = window_get_root_layer(window);
	window_layer = layer_create(WINDOW_FRAME);
	layer_add_child(window_root_layer, window_layer);

	settings = data_framework_get_settings();

	#ifdef PBL_ROUND
	GFont gothamLight = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_GOTHAM_LIGHT_34));
	GFont gothamBoldDefault = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_GOTHAM_BOLD_34));
	#else
	GFont gothamLight = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_GOTHAM_LIGHT_32));
	GFont gothamBoldDefault = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_GOTHAM_BOLD_32));
	#endif

	int width = WINDOW_SIZE.w-(padding*2);
	#ifdef PBL_ROUND
	GTextAlignment alignment = GTextAlignmentCenter;
	#else
	GTextAlignment alignment = GTextAlignmentLeft;
	#endif

	first_word_layer = text_layer_init(GRect(padding, 0+verticalShift, width, 168), alignment, gothamLight);
	layer_add_child(window_layer, text_layer_get_layer(first_word_layer));

	swear_word_layer = text_layer_init(GRect(padding, 30+verticalShift, width, 168), alignment, gothamBoldDefault);
	layer_add_child(window_layer, text_layer_get_layer(swear_word_layer));

	third_word_layer = text_layer_init(GRect(padding, 60+verticalShift, width, 168), alignment, gothamLight);
	layer_add_child(window_layer, text_layer_get_layer(third_word_layer));

	fourth_word_layer = text_layer_init(GRect(padding, 90+verticalShift, width, 168), alignment, gothamLight);
	layer_add_child(window_layer, text_layer_get_layer(fourth_word_layer));

	slot_layer = text_layer_init(GRect(padding, PBL_IF_ROUND_ELSE(142, 130), width, 168), alignment, PBL_IF_ROUND_ELSE(fonts_get_system_font(FONT_KEY_GOTHIC_18), fonts_get_system_font(FONT_KEY_GOTHIC_24)));
	layer_add_child(window_layer, text_layer_get_layer(slot_layer));

	graphics_layer = layer_create(WINDOW_FRAME);
	layer_set_update_proc(graphics_layer, graphics_layer_proc);
	layer_add_child(window_layer, graphics_layer);

	GBitmap *bluetoothIcon = gbitmap_create_with_resource(RESOURCE_ID_LIGNITE_IMAGE_BLUETOOTH_ICON);

	disconnect_notification = notification_create(window);
	notification_set_icon(disconnect_notification, bluetoothIcon);
	notification_set_accent_colour(disconnect_notification, PBL_IF_COLOR_ELSE(GColorRed, GColorWhite));
	notification_set_contents(disconnect_notification, "Oh boy...", "I lost connection to\nyour phone, sorry!");

    reconnect_notification = notification_create(window);
	notification_set_icon(reconnect_notification, bluetoothIcon);
	notification_set_accent_colour(reconnect_notification, PBL_IF_COLOR_ELSE(GColorBlue, GColorWhite));
	notification_set_contents(reconnect_notification, "Woohoo!", "You are now\nconnected to your\nphone again!");

    notification_register_push_handler(notification_push_handler);

	tick_timer_service_subscribe(MINUTE_UNIT, tick_handler);
	battery_state_service_subscribe(battery_handler);
	bluetooth_connection_service_subscribe(bluetooth_handler);
	accel_tap_service_subscribe(shake_handler);
	data_framework_register_settings_callback(main_window_settings_callback, SETTINGS_CALLBACK_MAIN_WINDOW);

	battery_handler(battery_state_service_peek());

	refresh_settings();

	struct tm *t;
  	time_t temp;
  	temp = time(NULL);
  	t = localtime(&temp);
	tick_handler(t, 0);

	app_timer_register(1800000, update_weather, NULL);

	//app_timer_register(1000, testing, NULL);
}

void window_unload_main(Window *window){
	text_layer_destroy(first_word_layer);
	text_layer_destroy(swear_word_layer);
	text_layer_destroy(third_word_layer);
	text_layer_destroy(fourth_word_layer);
	text_layer_destroy(slot_layer);
	layer_destroy(graphics_layer);
}

void main_window_init(){
	main_window = window_create();
	window_set_background_color(main_window, GColorBlack);
	window_set_window_handlers(main_window, (WindowHandlers){
		.load = window_load_main,
		.unload = window_unload_main,
	});
}

void main_window_deinit(){
	window_destroy(main_window);
}

Window *main_window_get_window(){
	return main_window;
}
