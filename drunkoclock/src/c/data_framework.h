#pragma once

#define NSLog(fmt, args...)                                \
  app_log(APP_LOG_LEVEL_INFO, __FILE_NAME__, __LINE__, fmt, ## args);

#define NSWarn(fmt, args...)                                \
  app_log(APP_LOG_LEVEL_WARNING, __FILE_NAME__, __LINE__, fmt, ## args);

#define NSError(fmt, args...)                                \
  app_log(APP_LOG_LEVEL_ERROR, __FILE_NAME__, __LINE__, fmt, ## args);

#define APP_NAME "Drunk O' Clock"

#define LOG_ALL true

#define SETTINGS_KEY 0
#define STORAGE_VERSION_KEY 100
#define USER_WAS_1X_KEY 101
#define AMOUNT_OF_SETTINGS_CALLBACKS 1

typedef enum AppKey {
	//Do not delete these
    APP_KEY_SECURITY_EMAIL = 1000,
    APP_KEY_SECURITY_ACCESS_CODE = 1001,
    APP_KEY_SECURITY_CHECKSUM = 1002,
    //Do not delete these ^

    APP_KEY_BTDISALERT = 0,
    APP_KEY_BTREALERT,
    APP_KEY_SOBER_MODE,
    APP_KEY_SHAKE_USE,
    APP_KEY_SLOT_USE,
    APP_KEY_TIME_COLOUR,
    APP_KEY_SWEAR_COLOUR,
    APP_KEY_SLOT_COLOUR,
    APP_KEY_GRAPHICS_COLOUR,
    APP_KEY_BACKGROUND_COLOUR,
    APP_KEY_BATTERY_BAR,

    APP_KEY_GS_DATE_FORMAT = 2000,

	APP_KEY_ICON = 100,
	APP_KEY_TEMPERATURE = 101
} AppKey;

typedef enum SettingsCallback {
	SETTINGS_CALLBACK_MAIN_WINDOW = 0
} SettingsCallback;

typedef enum WeatherStatus {
	WeatherStatusClearDay = 0,
	WeatherStatusClearNight,
	WeatherStatusWindy,
	WeatherStatusCold, //wut
	WeatherStatusPartiallyCloudyDay,
	WeatherStatusPartiallyCloudyNight,
	WeatherStatusHaze,
	WeatherStatusLightHaze,
	WeatherStatusRain,
	WeatherStatusSnowing,
	WeatherStatusHail,
	WeatherStatusCloudy,
	WeatherStatusStorm,
	WeatherStatusError
} WeatherStatus;

typedef struct Weather {
	int16_t temperature;
	WeatherStatus icon;
} Weather;

typedef enum {
	SlotUseDate = 0,
	SlotUseBluetoothStatus,
	SlotUseTemperature,
    SlotUseBatteryLevel,
    SlotUseDigitalTime,
	SlotUseNothing
} SlotUse;

typedef enum {
	ShakeUseExtraScreen = 0,
	ShakeUseChangeSwear,
	ShakeUseHide,
	ShakeUseDisabled
} ShakeUse;

typedef enum {
    GraphicsUseBatteryBar = 0,
    GraphicsUseSeconds
} GraphicsUse;

typedef struct Settings {
    bool btdisalert;
    bool btrealert;
    bool soberMode;
    bool batteryBar;

    ShakeUse shakeUse;
    SlotUse slotUse;

    GColor timeColour;
    GColor swearColour;
    GColor slotColour;
    GColor backgroundColour;
    GColor graphicsColour;

    Weather currentWeather;
    char dateFormat[1][16];
} Settings;

int get_random_int(bool specific, int specificTop);
char *get_main_swear_word(bool altBuffer, bool generateNew);
void send_weather_request();

typedef void (*SettingsChangeCallback)(Settings settings);

Settings data_framework_get_default_settings();

void data_framework_setup();
void data_framework_finish();
void process_tuple(Tuple *t);
void data_framework_inbox_dropped(AppMessageResult reason, void *context);
void data_framework_inbox(DictionaryIterator *iter, void *context);
void data_framework_log_settings_struct(Settings settings);
Settings data_framework_get_settings();
unsigned int data_framework_hex_string_to_uint(char const* hexstring);
void data_framework_register_settings_callback(SettingsChangeCallback callback, SettingsCallback callbackIdentity);

extern Settings data_framework_local_settings;
extern SettingsChangeCallback settings_callbacks[AMOUNT_OF_SETTINGS_CALLBACKS];
