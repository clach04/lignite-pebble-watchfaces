#include <pebble.h>
#include "lignite.h"
#include "data_framework.h"

void settings_setup();
void settings_finish();
Settings data_framework_get_default_settings();
void data_framework_save_settings(bool logResult);

Settings data_framework_local_settings;
SettingsChangeCallback settings_callbacks[AMOUNT_OF_SETTINGS_CALLBACKS];

uint32_t data_framework_checksum = 0;
bool userWas1x, doNotVibrate; //If the user used this face before 2.0

void data_framework_setup(){
    app_message_register_inbox_received(data_framework_inbox);
    app_message_register_inbox_dropped(data_framework_inbox_dropped);
    app_message_open(512, 512);

    settings_setup();
}

void data_framework_finish(){
    settings_finish();
}

void process_tuple(Tuple *t){
    int key = t->key;
    int value = t->value->int32;
    //NSLog("Got key %d with value %d", key, value);
    //NSLog("Settings: Got key %d with value %d", key, value);
    switch (key) {
        case APP_KEY_BTDISALERT:
            data_framework_local_settings.btdisalert = value;
            break;
        case APP_KEY_BTREALERT:
            data_framework_local_settings.btrealert = value;
            break;
        case APP_KEY_SOBER_MODE:
            data_framework_local_settings.soberMode = value;
            break;
        case APP_KEY_SHAKE_USE:
            data_framework_local_settings.shakeUse = value;
            break;
        case APP_KEY_SLOT_USE:
            data_framework_local_settings.slotUse = value;
            break;
        case APP_KEY_BATTERY_BAR:
            data_framework_local_settings.batteryBar = value;
            break;

        case APP_KEY_GS_DATE_FORMAT:
            strncpy(data_framework_local_settings.dateFormat[0], t->value->cstring, sizeof(data_framework_local_settings.dateFormat[0]));
            break;

        case APP_KEY_ICON:
            doNotVibrate = true;
            data_framework_local_settings.currentWeather.icon = value;
            break;
        case APP_KEY_TEMPERATURE:
            data_framework_local_settings.currentWeather.temperature = value;
            break;

        #ifdef PBL_COLOR
        case APP_KEY_TIME_COLOUR:
            data_framework_local_settings.timeColour = GColorFromHEX(data_framework_hex_string_to_uint(t->value->cstring));
            break;
        case APP_KEY_BACKGROUND_COLOUR:
            data_framework_local_settings.backgroundColour = GColorFromHEX(data_framework_hex_string_to_uint(t->value->cstring));
            break;
        case APP_KEY_SWEAR_COLOUR:
            data_framework_local_settings.swearColour = GColorFromHEX(data_framework_hex_string_to_uint(t->value->cstring));
            break;
        case APP_KEY_SLOT_COLOUR:
            data_framework_local_settings.slotColour = GColorFromHEX(data_framework_hex_string_to_uint(t->value->cstring));
            break;
        case APP_KEY_GRAPHICS_COLOUR:
            data_framework_local_settings.graphicsColour = GColorFromHEX(data_framework_hex_string_to_uint(t->value->cstring));
            break;
        #endif

        default:
            doNotVibrate = true;
            break;

        //Pebble.openURL("https://web.lignite.me:8443/settings/?pebbleAccountToken=04d29190d174877478b3043b948ddc19&app=modulite&email=edwin@lignite.io&accessCode=WOCAOZHONG");
    }
}

void data_framework_inbox(DictionaryIterator *iter, void *context){
    ////NSLog("Got a message");
    Tuple *t = dict_read_first(iter);
    if(t){
        process_tuple(t);
    }
    while(t != NULL){
        t = dict_read_next(iter);
        if(t){
            process_tuple(t);
        }
    }

    data_framework_save_settings(LOG_ALL);

    for(int i = 0; i < AMOUNT_OF_SETTINGS_CALLBACKS; i++){
        //data_framework_log_settings_struct(data_framework_local_settings);
        ////NSLog("\n---Calling back for %d---\n", i);
        settings_callbacks[i](data_framework_local_settings);
    }
    if(!doNotVibrate){
        ////NSLog("Got actual settings");
        vibes_double_pulse();
    }
    doNotVibrate = false;
    //04d29190d174877478b3043b948ddc19
}

void data_framework_inbox_dropped(AppMessageResult reason, void *context){
    if(LOG_ALL){
        ////NSLog("Message dropped, reason %d.", reason);
    }
}

void data_framework_register_settings_callback(SettingsChangeCallback callback, SettingsCallback callbackIdentity){
    ////NSLog("Registered callback for %d", callbackIdentity);
    settings_callbacks[callbackIdentity] = callback;
}

//Cheers to http://forums.getpebble.com/discussion/comment/132931#Comment_132931
unsigned int data_framework_hex_string_to_uint(char const* hexstring){
    unsigned int result = 0;
    char const *c = hexstring;
    unsigned char thisC;
    while((thisC = *c) != 0){
        thisC = toupper(thisC);
        result <<= 4;
        if(isdigit(thisC)){
            result += thisC - '0';
        }
        else if(isxdigit(thisC)){
            result += thisC - 'A' + 10;
        }
        else{
            APP_LOG(APP_LOG_LEVEL_DEBUG, "ERROR: Unrecognised hex character \"%c\"", thisC);
            return 0;
        }
        ++c;
    }
    return result;
}

int get_random_int(bool specific, int specificTop){
	int value = 1;
	if(!specific){
		value = rand() % 5;
	}
	else{
		value = rand() % specificTop;
	}
	return value;
}

Settings data_framework_get_settings(){
    #ifdef DEBUG_DEFAULT_SETTINGS
    #pragma message("Default settings are being used")
    return data_framework_get_default_settings();
    #else
    return data_framework_local_settings;
    #endif
}

Settings data_framework_get_default_settings(){
    Settings defaults;

    defaults.btdisalert = true;
    defaults.btrealert = false;
    defaults.soberMode = false;
    defaults.batteryBar = false;

    defaults.timeColour = GColorWhite;
    defaults.swearColour = GColorWhite;
    defaults.slotColour = GColorWhite;
    defaults.backgroundColour = GColorBlack;
    defaults.graphicsColour = GColorWhite;
    defaults.shakeUse = ShakeUseChangeSwear;
    defaults.slotUse = SlotUseDate;

    strncpy(defaults.dateFormat[0], "%A", sizeof(defaults.dateFormat[0]));

    return defaults;
}

#define CURRENT_STORAGE_VERSION 1

void data_framework_save_settings(bool logResult){
    int result = persist_write_data(SETTINGS_KEY, &data_framework_local_settings, sizeof(data_framework_local_settings));
    int result1 = persist_write_int(STORAGE_VERSION_KEY, CURRENT_STORAGE_VERSION);
    if(logResult){
        //NSLog("Wrote %d bytes to %s's settings.", result + result1, APP_NAME);
    }
}

int data_framework_load_settings(bool logResult){
    int result = persist_read_data(SETTINGS_KEY, &data_framework_local_settings, sizeof(data_framework_local_settings));
    if(logResult){
        //NSLog("Read %d bytes from %s's settings.", result, APP_NAME);
    }
    if(result < 0){
        data_framework_local_settings = data_framework_get_default_settings();
    }
    return result;
}

void settings_setup() {
    uint8_t version = persist_read_int(STORAGE_VERSION_KEY);

    int result = data_framework_load_settings(LOG_ALL);

    //User was already using the watchface and then upgraded to 2.0
    if(result > 0 && version == 0){
        persist_write_bool(USER_WAS_1X_KEY, true);
        data_framework_local_settings = data_framework_get_default_settings();
        userWas1x = true;
        return;
    }
    //or, the user had a good version in place
    if(version >= CURRENT_STORAGE_VERSION){
        userWas1x = persist_read_bool(USER_WAS_1X_KEY);
        return;
    }

    data_framework_local_settings = data_framework_get_default_settings();
}

void settings_finish(){
    data_framework_save_settings(LOG_ALL);
}