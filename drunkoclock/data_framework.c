#include <pebble.h>
#include "extras.h"
#include "data_framework.h"
#include "main_window.h"
#include "phrases.h"
#include "extra_window.h"

Settings settings;
DOCVersion version;
AppTimer *refresh_timer, *weather_refresh_timer, *message_timer;

/*
OUTDATED WORDPACKS
Used for porting 1.x users to 2.x
*/

wordpack sober = {
	.pack = {
		"flippin", "frikkin", "darnit", "bangin", "truckin", "crappin", "bleepin", "bleepin",
	},
};

wordpack tipsy = {
	.pack = {
		"shittin", "cocking", "twattin", "jizzing", "pissing", "bastard", "freakin", "damn",
	},
};

wordpack wasted = {
	.pack = {
		"fucking", "bitchin", "shittin", "fisting", "friggin", "fuckity", "unused", "unused",
	},
};

/*
END OUTDATED WORDPACKS
*/

void refresh_callback(){
	notify_bar_push_notif("Settings updated.", 2);
	main_window_update_settings(settings);
	phrases_set_settings(settings);
}

void weather_refresh_callback(){
	notify_bar_push_notif("Weather updated.", 0);
	main_window_update_settings(settings);
	extra_window_update_settings_raw(settings);
}

void process_tuple(Tuple *t){
	int key = t->key;
	int value = t->value->int32;
  switch (key) {
	  case 0:
		settings.invert = false;
	  	break;
	  case 1:
	  	settings.wordPack = value;
	  	break;
	  case 2:
	  	settings.progressiveDrunk = value;
	  	break;
	  case 3:
	  	settings.animation = value;
	  	break;
	  case 4:
	  	settings.dateFormat = value;
	  	break;
	  case 5:
	  	settings.seconds = value;
	  	break;
	  case 6:
	  	settings.shakeAction = value;
	  	break;
	  case 7:
	  	settings.batteryBar = value;
	  	break;
	  case 8:
	  	settings.btReAlert = value;
	  	break;
	  case 9:
	  	settings.btDisAlert = value;
	  	break;
	  case 10:
	  	settings.slotUse = value;
	  	break;
	  case 11:
	  	strcpy(settings.customslottext[0], t->value->cstring);
	  	break;
	  case 32:
	  	settings.updateFreq = value;
	  	break;
	  case 33:
	  	app_timer_cancel(refresh_timer);
	  	settings.previousTemp = value;
	  	weather_refresh_timer = app_timer_register(500, weather_refresh_callback, NULL);
	  	break;
	  case 34:
	  	settings.previousCondition = value;
	  	break;
	  case 35:
	  	settings.alerts = value;
	  	break;
	  default:
	  	//APP_LOG(APP_LOG_LEVEL_ERROR, "Got key %d with string %s", key, t->value->cstring);
	  	phrases_strcpy(key, t->value->cstring);
	  	break;
  }
}

void inbox(DictionaryIterator *iter, void *context){
	refresh_timer = app_timer_register(500, refresh_callback, NULL);
	Tuple *t = dict_read_first(iter);
	if(t){
		process_tuple(t);
	}
	while(t != NULL){
		t = dict_read_next(iter);
		if(t){
			process_tuple(t);
		}
	}
}

Settings get_settings(){
	return settings;
}

void write_settings(Settings newSettings){
	settings = newSettings;
}

int get_random_int(bool specific, int specificTop){
	int value = 1;
	if(!specific){
		value = rand() % 6;
	}
	else{
		value = rand() % specificTop;
	}
	return value;
}

void send_version_request(){
	DictionaryIterator *iter;
	app_message_outbox_begin(&iter);

	if (iter == NULL) {
	  return;
	}

	dict_write_uint16(iter, 200, 201);
	dict_write_end(iter);

	app_message_outbox_send();
}

int load_settings(){
	int valueRead = 0;
	if(persist_exists(KEY_SETTINGS) && persist_exists(KEY_VERSION)){
		valueRead += persist_read_data(KEY_SETTINGS, &settings, sizeof(settings));
		valueRead += persist_read_data(KEY_VERSION, &version, sizeof(version));
		APP_LOG(APP_LOG_LEVEL_INFO, "Using version %d.", version.versionCode);
	}
	else if(persist_exists(KEY_SETTINGS) && !persist_exists(KEY_VERSION)){
		APP_LOG(APP_LOG_LEVEL_INFO, "Settings exists, version key doesn't, must be coming from 1.x");
		persist oldSettings;
		valueRead += persist_read_data(KEY_SETTINGS, &oldSettings, sizeof(oldSettings));
		settings.updateFreq = 5;
		if(oldSettings.extraScreen == 1){
			settings.shakeAction = 0;
		}
		else{
			settings.shakeAction = SHAKE_USE_DISABLED;
		}
		settings.wordPack = oldSettings.wordPack-1;
		settings.slotUse = oldSettings.slotUse;
		settings.previousCondition = oldSettings.previousIcon;
		settings.dateFormat = oldSettings.dateFormat;
		settings.animation = 1;
		settings.batteryBar = oldSettings.batteryBar;
		settings.invert = oldSettings.theme;
		settings.seconds = oldSettings.secondsEnabled;
		if(oldSettings.btDisAlert != 0){
			settings.btDisAlert = true;
		}
		if(oldSettings.btReAlert != 0){
			settings.btReAlert = true;
		}
		settings.fahrenheit = oldSettings.tempPref;
		settings.progressiveDrunk = oldSettings.progressiveDrunk;
		settings.alerts = true;
		strcpy(settings.customslottext[0], oldSettings.customslottext[0]);

		if(persist_exists(KEY_WORDPACK_SOBER)){
			valueRead += persist_read_data(KEY_WORDPACK_SOBER, &sober, sizeof(sober));
		}
		if(persist_exists(KEY_WORDPACK_TIPSY)){
			valueRead += persist_read_data(KEY_WORDPACK_TIPSY, &tipsy, sizeof(tipsy));
		}
		if(persist_exists(KEY_WORDPACK_WASTED)){
			valueRead += persist_read_data(KEY_WORDPACK_WASTED, &wasted, sizeof(wasted));
		}

		for(int i = 0; i < 6; i++){
			APP_LOG(APP_LOG_LEVEL_INFO, "Copying %d: %s, %s, %s", i, sober.pack[i], tipsy.pack[i], wasted.pack[i]);
			phrases_strcpy(i+14, sober.pack[i]);
			phrases_strcpy(i+20, tipsy.pack[i]);
			phrases_strcpy(i+26, wasted.pack[i]);
		}
		int ind = phrases_save();

		message_timer = app_timer_register(2000, send_version_request, NULL);

		ind += persist_write_data(KEY_SETTINGS, &settings, sizeof(settings));

		APP_LOG(APP_LOG_LEVEL_DEBUG, "%d theme, %d seconds, %d btDisAlert, %d VALUEREAD total, %d IND save", oldSettings.theme, oldSettings.secondsEnabled, oldSettings.btDisAlert, valueRead, ind);
	}
	else{
		APP_LOG(APP_LOG_LEVEL_DEBUG, "Welcome brand new user :)");
		settings.updateFreq = 5;
		settings.shakeAction = 0;
		settings.wordPack = 1;
		settings.slotUse = 1;
		settings.previousCondition = 1;
		settings.dateFormat = 0;
		settings.animation = 1;
		settings.batteryBar = true;
		settings.invert = false;
		settings.seconds = false;
		settings.btDisAlert = true;
		settings.btReAlert = false;
		settings.fahrenheit = false;
		settings.progressiveDrunk = false;
		settings.alerts = true;
	}
	return valueRead;
}

int save_settings(){
	version.versionCode = CURRENT_VERSION;

	int total = persist_write_data(KEY_SETTINGS, &settings, sizeof(settings));
	total += persist_write_data(KEY_VERSION, &version, sizeof(version));
	return total;
}

void send_weather_request(){
  DictionaryIterator *iter;
  app_message_outbox_begin(&iter);

  if (iter == NULL) {
    return;
  }

  dict_write_uint16(iter, 200, 202);
  dict_write_end(iter);

  app_message_outbox_send();
}
