#include <pebble.h>
#include "extras.h"
#include "data_framework.h"

GFont gothamLight, gothamBoldDefault, gothamBoldSmall1, gothamBoldSmall2, gothamBoldTiny;
bool currentlyNotifying = false;
TextLayer *notif_bar_textlayer;
AppTimer *update_font_timer;
bool notify_bar_enabled = true;
int y_coord = 0;
int animation;

int get_random(int min, int max){
	int randToReturn = 0;
	while(randToReturn < min){
		randToReturn = rand() % max+1;
	}
	return randToReturn;
}

void set_animation_use(int use){
	animation = use;
}

void fire_animation(TextLayer *text_layer, int offset, GRect nextFrame, bool swear_word){
	Layer *layer = text_layer_get_layer(text_layer);
	GRect frame = layer_get_frame(layer);
	GRect finalDest = frame;
	GRect startDest = frame;
	switch(animation){
		case ANIMATION_CLASSIC:
			finalDest.origin.x = 144;
			startDest.origin.x = -144;
			break;
		case ANIMATION_VERTICLE:
			finalDest.origin.y = -200;
			startDest.origin.y = 200;
			break;
		case ANIMATION_WACKY:
			finalDest.origin.y = get_random(140, 300);
			startDest.origin.y = get_random(140, 300);
			finalDest.origin.x = get_random(145, 300);
			startDest.origin.x = -168;
			break;
		case ANIMATION_SHAKEUP:
			finalDest.origin.y = 10;
			startDest.origin.y = 140;
			finalDest.origin.x = 0;
			startDest.origin.x = 0;
			break;
	}
	if(animation != ANIMATION_SHAKEUP){
		animate_layer(layer, &frame, &finalDest, 300, 300 + offset);
		animate_layer(layer, &startDest, &nextFrame, 300, 1100 + offset);
	}
	else{
		animate_layer(layer, &frame, &startDest, 100, 400+offset);
		animate_layer(layer, &startDest, &finalDest, 100, 500+offset);
		animate_layer(layer, &finalDest, &startDest, 100, 600+offset);
		animate_layer(layer, &startDest, &finalDest, 100, 700+offset);
		animate_layer(layer, &finalDest, &startDest, 100, 800+offset);
		animate_layer(layer, &startDest, &finalDest, 100, 900+offset);
		animate_layer(layer, &finalDest, &startDest, 100, 1000+offset);
		animate_layer(layer, &startDest, &finalDest, 100, 1100+offset);
		animate_layer(layer, &finalDest, &nextFrame, 100, 1200+offset);
	}
	if(swear_word){
		y_coord = nextFrame.origin.y;
		update_font_timer = app_timer_register(1000, update_font_callback, layer);
	}
}

void create_fonts(){
	gothamLight = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_GOTHAM_LIGHT_34));
	gothamBoldDefault = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_GOTHAM_BOLD_36));
	gothamBoldSmall1 = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_GOTHAM_BOLD_22));
	gothamBoldSmall2 = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_GOTHAM_BOLD_16));
	gothamBoldTiny = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_GOTHAM_BOLD_13));
}

GRect get_new_frame(TextLayer* layer, char *new_word){
	size_t length = strlen(new_word);
	GRect frame = layer_get_frame(text_layer_get_layer(layer));
	if(length < 8){
		frame.origin.y = 30;
	}
	else if(length > 7 && length < 12){
		frame.origin.y = 37;
	}
	else if(length > 11 && length < 15){
		frame.origin.y = 42;
	}
	else{
		frame.origin.y = 45;
	}
	return frame;
}

void update_font_callback(void *data){
	TextLayer *layer = data;
	adapt_font_size_to_frame(layer);
}

void adapt_font_size_to_frame(TextLayer *layer){
	if(y_coord == 0){
		GRect frame = layer_get_frame(text_layer_get_layer(layer));
		y_coord = frame.origin.y;
	}
	switch(y_coord){
		case 30:
			text_layer_set_font(layer, gothamBoldDefault);
			break;
		case 37:
			text_layer_set_font(layer, gothamBoldSmall1);
			break;
		case 42:
			text_layer_set_font(layer, gothamBoldSmall2);
			break;
		case 45:
			text_layer_set_font(layer, gothamBoldTiny);
			break;
	}
}

TextLayer* text_layer_init(GRect location, GTextAlignment alignment, int font){
	TextLayer *layer = text_layer_create(location);
	text_layer_set_text_color(layer, GColorWhite);
	text_layer_set_background_color(layer, GColorClear);
	text_layer_set_text_alignment(layer, alignment);
	switch(font){
		case 1:
			text_layer_set_font(layer, gothamLight);
			break;
		case 2:
			text_layer_set_font(layer, gothamBoldDefault);
			break;
		case 3:
			text_layer_set_font(layer, fonts_get_system_font(FONT_KEY_GOTHIC_24));
			break;
		case 4:
			text_layer_set_font(layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
			break;
	}
	return layer;
}

void stopped(Animation *anim, bool finished, void *context){
    property_animation_destroy((PropertyAnimation*) anim);
}

void animate_layer(Layer *layer, GRect *start, GRect *finish, int duration, int delay){
    PropertyAnimation *anim = property_animation_create_layer_frame(layer, start, finish);

    animation_set_duration((Animation*) anim, duration);
    animation_set_delay((Animation*) anim, delay);

    AnimationHandlers handlers = {
        .stopped = (AnimationStoppedHandler) stopped
    };
    animation_set_handlers((Animation*) anim, handlers, NULL);

    animation_schedule((Animation*) anim);
}

void notify_bar_set_enabled(bool enabled){
	notify_bar_enabled = enabled;
}

void notify_bar_create(Window *w){
	notif_bar_textlayer = text_layer_init(GRect(0, 300, 144, 168), GTextAlignmentCenter, 0);
	text_layer_set_font(notif_bar_textlayer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
	text_layer_set_text_color(notif_bar_textlayer, GColorWhite);
	text_layer_set_background_color(notif_bar_textlayer, GColorBlack);
	layer_add_child(window_get_root_layer(w), text_layer_get_layer(notif_bar_textlayer));
}

void notify_bar_push_notif(char *sentence, int vibrateNum){
	if(currentlyNotifying){
		return;
	}
	switch(vibrateNum){
		case 1:
			vibes_short_pulse();
			break;
		case 2:
			vibes_double_pulse();
			break;
		case 3:
			vibes_long_pulse();
			break;
	}
	if(!notify_bar_enabled){
		return;
	}
	text_layer_set_text(notif_bar_textlayer, sentence);
	currentlyNotifying = true;
	animate_layer(text_layer_get_layer(notif_bar_textlayer), &GRect(0, 300, 144, 168), &GRect(0, 145, 144, 168), 1000, 0);
	animate_layer(text_layer_get_layer(notif_bar_textlayer), &GRect(0, 145, 144, 168), &GRect(0, 300, 144, 168), 1000, 6000);
}
